import { Dispatch } from 'redux';
import { Request } from 'superagent';
import { getStore } from '../../redux/index';
import {
    appClearAuth,
    setRefreshData as setRefreshDataAction,
    setToken as setTokenAction,
    refreshToken as refreshTokenAction,
    userAuthByStoredData,
    authInitializeSuccess,
    setRefreshed
} from '../../redux/actions/auth';
import LOCAL_STORAGE_FIELDS from '../../constants/localStorageFields';
import { host } from '../../api';
import { TFunc } from '../../types/types';
import { IRootReducer } from '../../redux/reducers';
import { IRefreshData } from '../../redux/reducers/auth';
import { decode } from 'jsonwebtoken';
import moment from 'moment';
import { beginUpdateNotifs } from 'core/redux/actions/user';

let refresh_token_timeout_id: number;

export interface IAuthTokenService {
    getToken: () => string | undefined;
    setToken: (newToken: string) => void;
    clearToken: () => void;
    refreshToken: (req: Request) => void;
    getRefreshData: () => IRefreshData | undefined;
    clearRefreshData: () => void;
    setRefreshData: (newRefreshData: IRefreshData) => void;
    tokenPlugin: (req: Request) => void;
    stopRefreshToken: () => void;
    init: (dispatch: Dispatch) => void;
    hasTokenInLocalstorage: () => boolean;
}

const init = (dispatch: Dispatch): void => {
    const token = window.localStorage.getItem(LOCAL_STORAGE_FIELDS.TOKEN);
    const accessTokenLifeTime = window.localStorage.getItem(LOCAL_STORAGE_FIELDS.ACCESS_TOKEN_LIFETIME);
    const refreshTokenValue = window.localStorage.getItem(LOCAL_STORAGE_FIELDS.REFRESH_TOKEN);


    if (token && accessTokenLifeTime && refreshTokenValue) {
        if(moment((decode(refreshTokenValue) as any).exp * 1000).isAfter()) {
            console.log('[Auth] Start refresh')
            dispatch(refreshTokenAction());
            console.log('[Auth] Load refresh data from local storage')
            setRefreshData({
                refreshToken: refreshTokenValue,
                accessTokenLifeTime: Number(accessTokenLifeTime)
            }, false)
        } else {
            console.log('[Auth] Clear token. Refresh expired')
            clearToken();
            clearRefreshData();
        }
    } else {
        console.log('[Auth] No stored token')
    }

    dispatch(authInitializeSuccess());
};

const getToken = (): string | undefined => (getStore().getState() as IRootReducer).auth.token;
const getRefreshData = (): IRefreshData | undefined => (getStore().getState() as IRootReducer).auth.refreshData;

const hasTokenInLocalstorage = () => {
    return !!window.localStorage.getItem(LOCAL_STORAGE_FIELDS.TOKEN);
}

const setToken = (newToken: string): void => {
    const store = getStore();
    store.dispatch(setTokenAction({
        token: newToken
    }));
    window.localStorage.setItem(LOCAL_STORAGE_FIELDS.TOKEN, newToken);
};
const clearToken = (): void => {
    const store = getStore();
    store.dispatch(setTokenAction({
        token: undefined
    }));
    window.localStorage.removeItem(LOCAL_STORAGE_FIELDS.TOKEN);
    console.log('[Auth] Clear access token')
};

const setRefreshData = (newRefreshData: IRefreshData, setTimeout = true): void => {
    const store = getStore();
    store.dispatch(setRefreshDataAction({
        refreshData: newRefreshData
    }));
    window.localStorage.setItem(LOCAL_STORAGE_FIELDS.ACCESS_TOKEN_LIFETIME, String(newRefreshData.accessTokenLifeTime));
    window.localStorage.setItem(LOCAL_STORAGE_FIELDS.REFRESH_TOKEN, newRefreshData.refreshToken);
    if(setTimeout){
        stopRefreshToken();
        refresh_token_timeout_id = window.setTimeout(() => {
            store.dispatch(refreshTokenAction());
        }, (newRefreshData.accessTokenLifeTime || 300) * 900);
        console.log(`[Auth] Set refresh timer ${refresh_token_timeout_id}`)
        store.dispatch(setRefreshed())
    }
    console.log('[Auth] Set refresh data')
};

const clearRefreshData = (): void => {
    const store = getStore();
    store.dispatch(setRefreshDataAction({
        refreshData: undefined
    }));
    window.localStorage.removeItem(LOCAL_STORAGE_FIELDS.ACCESS_TOKEN_LIFETIME);
    window.localStorage.removeItem(LOCAL_STORAGE_FIELDS.REFRESH_TOKEN);
    console.log('[Auth] Clear refresh data')
};


const refreshToken = (req: Request): void => {
    req.on('response', (res: any) => {
        const refreshData = getRefreshData();

        if (refreshData?.refreshToken && res.status === 401) {
            const store = getStore();
            if (res.req.url === `${host}/auth/refresh`) {
                store.dispatch(appClearAuth());
                clearToken();
            } else {
                store.dispatch(refreshTokenAction());
            }
        }
    });
};

const stopRefreshToken: TFunc = () => {
    if (refresh_token_timeout_id) {
        console.log(`[Auth] clear refresh timer ${refresh_token_timeout_id}`)
        window.clearTimeout(refresh_token_timeout_id);
    }
};

const tokenPlugin = (req: Request): void => {
    const token = getToken();
    if (token) { req.set('Authorization', `Bearer ${token}`); }
};

export default {
    getToken,
    setToken,
    refreshToken,
    getRefreshData,
    setRefreshData,
    tokenPlugin,
    stopRefreshToken,
    init,
    clearToken,
    clearRefreshData,
    hasTokenInLocalstorage
} as IAuthTokenService;
