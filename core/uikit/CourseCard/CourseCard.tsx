import { ICourse } from 'core/redux/types/dto/course';
import { declOfNum } from 'core/utils/devlOfNum';
import Image from '../Image';
import Typo from '../Typo';
import styles from './CourseCard.module.scss';

interface IProps {
  data: ICourse;
  onClick?: () => void;
}

const CourseCard: React.FC<IProps> = ({
  data,
  onClick
}) => {
  return (
    <div onClick={onClick} className={styles.card}>
      <Image
        src={data.photo_url || 'https://api.m-dev.group/image/zFNHrKR/Frame-1.png'}
        alt=''
        className={styles.card__image}
      />
      <div className={styles.card__info}>
        <Typo variant='title-16-b'>Курс: {data.name}</Typo>
        <Typo variant='title-14'>Рекомендуемый возраст: {data.minAge} - {data.maxAge} {declOfNum(data.maxAge, ['год', 'года', 'лет'])}</Typo>
        <Typo variant='title-14'>Автор: {[data.teacher?.first_name, data.teacher?.last_name].join(' ')}</Typo>
        <Typo variant='title-14'>Описание: {data.description}</Typo>
      </div>
    </div>
  )
};

export default CourseCard;