import cn from 'classnames';
import { TFunc } from 'core/types/types';
import { isChild } from 'core/utils/isChild';
import { useEffect, useRef } from 'react';
import styles from './Dropdown.module.scss';

export interface IDropdownItem {
  label: string;
  onClick: TFunc;
}

interface IProps {
  items: IDropdownItem[];
  onClose: TFunc;
  classNames?: {
    main?: string;
    item?: string;
  },
  placement?: 'top' | 'bottom';
}

const Dropdown: React.FC<IProps> = ({
  items,
  onClose,
  classNames,
  placement = 'bottom'
}) => {
  const dropRef = useRef<HTMLDivElement>(null);

  useEffect(()=>{
    const handleClose = (e: MouseEvent) => {
      if(e.target && dropRef.current) {
        if(!isChild(e.target as HTMLElement, dropRef.current, 3)) {
          if(onClose) onClose()
        }
      }
    };

    window.addEventListener('click', handleClose);

    return ()=>{
      window.removeEventListener('click', handleClose);
    }
  }, [onClose]);

  return <div className={cn(styles.dropdown, classNames?.main, styles[`dropdown--placement-${placement}`])} ref={dropRef}>
    {items.map((item, index)=>(
      <div className={cn(styles.dropdown__item, classNames?.item)} key={index} onClick={item.onClick}>
        {item.label}
      </div>
    ))}
  </div>
}

export default Dropdown;