import classNames from 'classnames';
import { isChild } from 'core/utils/isChild';
import { MouseEventHandler, useCallback, useState } from 'react';
import Dropdown from '../Dropdown';
import Image from '../Image';
import Typo from '../Typo';
import styles from './Select.module.scss';

interface IItem {
  label: string;
  onClick: () => void;
}

interface IProps {
  items: IItem[];
  placeholder?: string;
  value?: string;
  label?: string;
  placement?: 'top' | 'bottom';
  className?: string;
}

const Select: React.FC<IProps> = ({
  placeholder,
  value,
  items,
  label,
  placement = 'bottom',
  className
}) => {
  const [isOpened, setOpened] = useState(false);

  const handleOpenDropdown: MouseEventHandler = useCallback((e) => {
    if(isChild(e.target as HTMLElement, e.currentTarget as HTMLElement, 2)) {
      setOpened(true);
    }
  }, [])

  const handleClick = useCallback((onClick) => () => {
    if(onClick) {
      onClick()
    }

    setOpened(false);
  }, [])

  return (
    <div>
      {label && <Typo variant='rubrik' className={styles.select__label}>{label}</Typo>}
      <div className={classNames(styles.select, className)} onClick={handleOpenDropdown}>
        {value ? (
          <span className={styles.select__value}>{value}</span>
        ) : (
          <span className={styles.select__placeholder}>{placeholder}</span>
        )}
        <Image
          src='/images/icons/chevron-bottom.svg'
          alt=''
          className={styles.select__chevron}
        />
        {isOpened && (
          <Dropdown
            classNames={{
              main: styles.select__dropdown
            }}
            onClose={() => setOpened(false)}
            items={items.map((item) => ({
              ...item,
              onClick: handleClick(item.onClick)
            }))}
            placement={placement}
          />
        )}
      </div>
    </div>
  )
};

export default Select;