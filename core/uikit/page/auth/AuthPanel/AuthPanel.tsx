import Link from 'next/link';
import Button from 'core/uikit/Button';
import Input from 'core/uikit/Input/Input';
import Typo from 'core/uikit/Typo';
import styles from './AuthPanel.module.scss';
import redirectTo from 'core/utils/redirectTo';
import { useForm } from 'core/hooks/useForm';
import { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { userAuth } from 'core/redux/actions/auth';
import { IRootReducer } from 'core/redux/reducers';
import ModalContainer from 'core/uikit/ModalContainer';

const AuthPanel: React.FC = () => {
  const dispatch = useDispatch();

  const authError = useSelector((store: IRootReducer) => store.auth.authError);

  const onSubmit = useCallback((formData) => {
    dispatch(userAuth(formData))
  }, [dispatch])
  
  const {
    formData,
    onChangeField,
    handleSubmit,
    reinit
  } = useForm(onSubmit)

  const [forgot, setForgot] = useState(false);

  return (
    <div className={styles.panel__wrapper}>
      {forgot && (
        <ModalContainer className={styles.forgotModal} onClose={() => {setForgot(false)}}>
          <h3>Техническая поддержка</h3>
          <p>
            Если вы забыли пароль, пожалуйста свяжитесь со службой технической поддержки.
          </p>
          <a href='mailto://contact@m-dev.group'>
            contact@m-dev.group
          </a>
        </ModalContainer>
      )}
      <div className={styles.panel}>
        <Typo variant='title-24'>
          Авторизация
        </Typo>

        <p className={styles.panel__change}>Или <Link href='/auth/register'>зарегистрируйтесь</Link> если еще нет аккаунта</p>
        <Button className={styles.panel__forgot} variant='transparent' onClick={() => setForgot(true)}>
          Забыли пароль?
        </Button>


        {authError && <Typo variant='title-14' className={styles.panel__error}>{authError}</Typo>}

        <div className={styles.panel__form}>
          <Input
            label='E-mail'
            placeholder='example@mail.com'
            value={formData['username']}
            onChange={onChangeField('username')}
            error={!!authError}
          />
          <Input
            label='Пароль'
            placeholder='********'
            isPassword={true}
            value={formData['password']}
            onChange={onChangeField('password')}
            error={!!authError}
          />
        </div>

        <div className={styles.panel__actions}>
          <Button variant='primary' onClick={handleSubmit}>
            Войти
          </Button>
        </div>
      </div>
    </div>
  )
}

export default AuthPanel;