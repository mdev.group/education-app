import Link from 'next/link';
import Button from 'core/uikit/Button';
import Input from 'core/uikit/Input/Input';
import Typo from 'core/uikit/Typo';
import styles from './RegisterPanel.module.scss';
import redirectTo from 'core/utils/redirectTo';
import Select from 'core/uikit/Select';
import { useForm } from 'core/hooks/useForm';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { userRegister } from 'core/redux/actions/auth';
import { IRootReducer } from 'core/redux/reducers';

const RegisterPanel: React.FC = () => {
  const dispatch = useDispatch();

  const registerError = useSelector((store: IRootReducer) => store.auth.registerError);

  const onSubmit = useCallback((formData) => {
    const [first_name, last_name] = formData.name.split(' ');
    
    dispatch(userRegister({
      first_name: first_name.trim(),
      last_name: last_name.trim(),
      email: formData.email.trim(),
      password: formData.password.trim(),
      user_type: formData.usertype
    }))
  }, [dispatch])
  
  const {
    formData,
    onChangeField,
    handleSubmit,
    reinit
  } = useForm(onSubmit, {})

  return (
    <div className={styles.panel__wrapper}>
      <div className={styles.panel}>
        <Typo variant='title-24'>
          Регистрация
        </Typo>

        <p className={styles.panel__change}>Или <Link href='/auth'>войдите</Link> если уже есть аккаунт</p>

        <div className={styles.panel__form}>
          <Input
            label='Имя Фамилия'
            placeholder='Иван Иванов'
            value={formData['name']}
            onChange={onChangeField('name')}
          />
          <Input
            label='e-mail'
            placeholder='example@mail.com'
            value={formData['email']}
            onChange={onChangeField('email')}
          />
          <Input
            label='Пароль'
            placeholder='********'
            value={formData['password']}
            onChange={onChangeField('password')}
          />
          <Select
            label='Роль'
            placeholder='Не выбрано'
            items={[
              {
                label: 'Учитель',
                onClick: () => onChangeField('usertype')('teacher')
              },
              {
                label: 'Ученик',
                onClick: () => onChangeField('usertype')('pupil')
              }
            ]}
            value={(formData['usertype'] !== undefined || undefined) && (formData['usertype'] === 'teacher' ? 'Учитель' : 'Ученик')}
          />
        </div>

        {registerError && <Typo variant='title-14' className={styles.panel__error}>{registerError}</Typo>}

        <div className={styles.panel__actions}>
          <Button variant='secondary-border' onClick={() => redirectTo('/auth')}>
            Войти в аккаунт
          </Button>
          <Button variant='primary' onClick={handleSubmit}>
            Далее
          </Button>
        </div>
      </div>
    </div>
  )
}

export default RegisterPanel;