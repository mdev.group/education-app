import styles from "styles/pages/TaskPage.module.scss";

const typeAssign = {
  1: styles.field__water,
  2: styles.field__grass,
  3: styles.field__bridge,
  4: styles.field__cat,
  5: styles.field__cactus
} as {[key: number]: string}

export const mapCeilTypeToClassName = (type: number) => {
  return typeAssign[type];
}


export const MY_TOOLBOX = {

}