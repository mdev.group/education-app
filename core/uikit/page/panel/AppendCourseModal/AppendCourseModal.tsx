import { useForm } from "core/hooks/useForm";
import { beginCreateLevel, beginCreateTask, beginUpdateCourse } from "core/redux/actions/course";
import { ICourse } from "core/redux/types/dto/course";
import { ILevelDTO } from "core/redux/types/dto/level";
import { TFunc } from "core/types/types";
import Button from "core/uikit/Button";
import Input from "core/uikit/Input/Input";
import Select from "core/uikit/Select";
import Typo from "core/uikit/Typo";
import { useCallback, useState } from "react";
import { useDispatch } from "react-redux";

import styles from './AppendCourseModal.module.scss';

interface IProps {
  onClose: TFunc;
  course: ICourse;
}

const AppendCourseModal: React.FC<IProps> = ({
  onClose,
  course
}) => {
  const dispatch = useDispatch();
  const [isAddLevel, setAddLevel] = useState(true);
  const [selectedLevel, setLevel] = useState<ILevelDTO | null>(null);

  const handleCreate = useCallback((formData) => {
    if (isAddLevel) {
      dispatch(beginCreateLevel({
        courseID: course?._id,
        data: {
          order: 0,
          name: formData.name,
          courseID: course?._id,
        }
      }))
    } else {
      if(selectedLevel) {
        dispatch(beginCreateTask({
          data: {
            levelID: selectedLevel?._id,
            text: 'Текст задания',
            hint: 'Подсказка',
            name: formData.name,
          }
        }))
      }
    }

    if (onClose) onClose();
  }, [course, dispatch, isAddLevel, selectedLevel, onClose]);

  const {
    formData,
    onChangeField,
    handleSubmit
  } = useForm(handleCreate)

  return (
    <div className={styles.container}>
      <Typo variant='title-20'>
        Дополнение курса
      </Typo>

      <div className={styles.container__form}>
        <Select
          label="Тип"
          items={[
            {
              label: 'Глава',
              onClick: () => setAddLevel(true)
            },
            {
              label: 'Задание',
              onClick: () => setAddLevel(false)
            }
          ]}
          placeholder="Не выбрано"
          className={styles.container__select}
          value={isAddLevel ? 'Глава' : 'Задание'}
        />

        {!isAddLevel && (
          <Select
            label="Глава задания"
            items={(course.levels || []).map((level) => ({
              label: level.name,
              onClick: () => setLevel(level)
            }))}
            placeholder="Не выбрано"
            className={styles.container__select}
            value={selectedLevel?.name}
          />
        )}

        <Input
          label="Название"
          value={formData['name']}
          onChange={onChangeField('name')}
          placeholder={isAddLevel ? 'Глава N' : 'Задание N'}
        />
      </div>

      <div className={styles.container__actions}>
        <Button
          variant='primary'
          onClick={handleSubmit}
          disabled={isAddLevel ? (!formData.name) : (!formData.name || !selectedLevel)}
        >
          Создать
        </Button>
      </div>
    </div>
  )
};

export default AppendCourseModal;