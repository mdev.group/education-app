import classNames from "classnames";
import { useForm } from "core/hooks/useForm";
import { beginGetCourse, beginGetGlobalCourses, beginGetOwnedCourses, successGetCourse } from "core/redux/actions/course";
import { getMyGroup } from "core/redux/actions/group";
import { beginUpdateUser } from "core/redux/actions/user";
import { currentUser_selector, isTeacher_selector, myGroup_selector, ownedCourses_selector } from "core/redux/selectors";
import { ILevelDTO } from "core/redux/types/dto/level";
import { IPupil } from "core/redux/types/dto/pupil";
import Button from "core/uikit/Button";
import Image from "core/uikit/Image";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import Select from "core/uikit/Select";
import Typo from "core/uikit/Typo";
import { declOfNum } from "core/utils/devlOfNum";
import { calculeteScore, getActiveTask, getCurrentLevel, getScoresByUser } from "core/utils/getScoresByTaskID";
import moment from "moment";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProgressStudents from "../TeacherPage/components/ProgressStudents/ProgressStudents";
import ProgressDetails from "./componetns/ProgressDetails/ProgressDetails";

import styles from './StudentPage.module.scss';

const StudentPage: React.FC = () => {
  const dispatch = useDispatch();

  const [selectedCourseID, setSelectedCourse] = useState<string | null>(null);

  const [modalOpened, setModalOpened] = useState(false);

  const user = useSelector(currentUser_selector) as IPupil;
  const myGroup = useSelector(myGroup_selector);
  const ownedCourses = useSelector(ownedCourses_selector);
  const isTeacher = useSelector(isTeacher_selector);

  const userName = useMemo(() => [user?.first_name, user?.last_name].join(' '), [user])
  const userAge = useMemo(() => moment().diff(user?.birth_date, 'years'), [user]);

  const currentCourse = useMemo(() => selectedCourseID ? ownedCourses.find((item) => item._id === selectedCourseID) : undefined, [ownedCourses, selectedCourseID])

  useEffect(() => {
    if(currentCourse) {
      dispatch(successGetCourse(currentCourse))
    }
  }, [currentCourse, dispatch])

  const handleDeletePhoto = useCallback(() => {
    dispatch(beginUpdateUser({
      photo_url: null,
    }))
  }, [dispatch])

  const onSubmit = useCallback((formData) => {
    const [first_name, last_name] = formData.name.split(' ');

    dispatch(beginUpdateUser({
      photo_url: formData.photo_url,
      first_name: first_name,
      last_name: last_name,
      username: formData.username,
      birth_date: formData.birth_date
    }))
    
    setModalOpened(false);
  }, [dispatch])
  
  const {
    formData,
    onChangeField,
    handleSubmit,
    reinit
  } = useForm(onSubmit, {
    ...user,
    name: [user?.first_name, user?.last_name].join(' '),
  })

  useEffect(() => {
    reinit();
  }, [user])

  useEffect(() => {
    if(user?.groupID && user?.groupID !== myGroup?._id){
      dispatch(getMyGroup({
        id: user.groupID
      }))
    }

    if(myGroup && !selectedCourseID){
      setSelectedCourse(myGroup.courseID);
    }
  }, [dispatch, user, myGroup, selectedCourseID])

  const {
    level,
    task,
    levelProgress
  } = useMemo(() => {
    if(user && currentCourse) { return getActiveTask(user, currentCourse) }

    return {
      level: null,
      task: null,
      levelProgress: 0
    };
  }, [currentCourse, user])

  useEffect(() => {
    if(user) {
      dispatch(beginGetOwnedCourses({
        userType: isTeacher ? 'teacher' : 'student'
      }))

      dispatch(beginGetGlobalCourses())
    }
  }, [dispatch, isTeacher, user]);

  const handleCourseSelect = useCallback((courseID) => () => {
    setSelectedCourse(courseID)
  }, [])


  const studentScore = useMemo(() => user && ownedCourses ? getScoresByUser(user, ownedCourses).scores : 0, [user, ownedCourses])
  const {
    progress,
    currentLevel
  } = useMemo(() => calculeteScore(studentScore), [studentScore])


  return (
    <>
      {modalOpened && (
        <ModalContainer
          onClose={() => setModalOpened(false)}
          className={styles.page__modal}
        >
          <ModalForm className={styles.page__modalForm}>
            <Typo variant='title-20'>Настройка аккаунта</Typo>
            <ImageUploader
              label={(
                <>
                  <span className={styles['page__text--green']}>Выберите фото</span>
                  <span>{' '}или перетащите в эту область</span>
                </>
              )}
              onUpload={onChangeField('photo_url')}
              value={formData['photo_url']}
            />
            <Input
              label="Имя фамилия"
              value={formData['name']}
              onChange={onChangeField('name')}
            />
            <Input
              label="e-mail"
              value={formData['username']}
              onChange={onChangeField('username')}
            />
            <Input
              label="Дата рождения"
              type="date"
              value={formData['birth_date']}
              onChange={onChangeField('birth_date')}
            />
            <div className={styles.page__modalActions}>
              <Button
                variant='primary'
                onClick={handleSubmit}
              >
                Сохранить
              </Button>
              <Button
                variant='secondary-border'
                onClick={() => {reinit(); setModalOpened(false)}}
              >
                Отмена
              </Button>
            </div>
          </ModalForm>
        </ModalContainer>
      )}
      <div className={styles.page__container}>
        <div className={styles.page__row}>
          <div className={styles.page__baseInfoWrapper}>
            <div className={styles.page__userPhotoWrapper}>
              <Image
                className={styles.page__userPhoto}
                src={user?.photo_url || "/images/icons/blank/user.svg"}
                alt=""
              />
              <div className={styles.page__userPhotoOverlay}>
                <Button
                  onClick={() => setModalOpened(true)}
                  variant='transparent'
                  className={styles.page__photoAction}
                >
                  <Image
                    src="/images/icons/pen.svg"
                    alt="edit"
                    className={styles.page__photoActionIcon}
                  />
                </Button>

                <Button
                  onClick={handleDeletePhoto}
                  variant='transparent'
                  className={styles.page__photoAction}
                >
                  <Image
                    src="/images/icons/trash.svg"
                    alt="delete"
                    className={styles.page__photoActionIcon}
                  />
                </Button>
              </div>
            </div>
            <div className={styles.page__baseInfo}>
              <Typo variant='title-20'>{userName}</Typo>
              <Typo variant='title-14'>Почта: {user?.username}</Typo>
              {userAge > 0 && <Typo variant='title-14'>Возраст: {userAge} {declOfNum(userAge, ['Год', 'Года', 'Лет'])}</Typo>}
              <Typo variant='title-14'>Статус: Ученик</Typo>

              <div className={styles.page__studentProgressbarWrapper}>
                <div className={styles.page__studentProgressbarInfo}>
                  <Typo variant='title-14'>{myGroup ? `Группа: ${myGroup?.name}` : 'Без группы'}</Typo>
                  <Typo variant='title-14'>Уровень {currentLevel}</Typo>
                </div>
                <progress
                  className={styles.page__studentProgressbar}
                  value={progress}
                />
              </div>
            </div>
          </div>

          <div className={styles.page__spacer} />

          <div className={styles.page__actions}>
            <Button
              onClick={() => setModalOpened(true)}
              variant='transparent'
              className={styles.page__action}
            >
              <Image
                src="/images/icons/pen.svg"
                alt="edit"
              />
            </Button>
          </div>
        </div>

        <div className={styles.page__row}>
          <div className={styles.page__cardWrapper}>
            <div className={styles.page__studentsGroupCard}>
              <ProgressDetails courseData={currentCourse}/>
            </div>
          </div>

          <div className={styles.page__courseSelector}>
            <div className={styles.page__courseSelectorCard}>
              <Typo variant='title-16-b'>Выбор курса</Typo>
              <Select
                items={ownedCourses.map((course) => ({
                  label: course.name,
                  onClick: handleCourseSelect(course._id)
                }))}
                placeholder = "Выберите на курс"
                value={currentCourse?.name}
              />
            </div>
          </div>

          <div className={styles.page__studentsProgressCardWrapper}>
            <Typo variant='title-20'>Результаты одногруппников</Typo>
            <div className={styles.page__studentsProgressCard}>
              <ProgressStudents />
            </div>
          </div>
        </div>
      </div>
    </>
  )
};

export default StudentPage;