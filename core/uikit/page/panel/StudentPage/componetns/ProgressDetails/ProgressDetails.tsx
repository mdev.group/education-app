import classNames from 'classnames';
import { beginAssignPupil, beginCourseAssignPupil } from 'core/redux/actions/pupil';
import { currentCourse_selector, currentUser_selector, isTeacher_selector } from 'core/redux/selectors';
import { ICourse } from 'core/redux/types/dto/course';
import { IPupil } from 'core/redux/types/dto/pupil';
import Button from 'core/uikit/Button';
import Image from 'core/uikit/Image';
import Typo from 'core/uikit/Typo';
import { getActiveTask } from 'core/utils/getScoresByTaskID';
import redirectTo from 'core/utils/redirectTo';
import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './ProgressDetails.module.scss';

interface IClasses {
  main?: string;
  list?: string;
};

interface IProps {
  courseData?: ICourse;
  classes?: IClasses;
  showTitle?: boolean;
  showAction?: boolean;
  showNames?: boolean;
}

const ProgressDetails: React.FC<IProps> = ({
  courseData: currentCourse,
  classes,
  showTitle = true,
  showAction = true,
  showNames = true
}) => {
  const dispatch = useDispatch();

  const isTeacher = useSelector(isTeacher_selector);
  const currentUser = useSelector(currentUser_selector) as IPupil;

  const handleAssign = useCallback(() => {
    dispatch(beginCourseAssignPupil({
      id: currentUser._id,
      courseID: currentCourse?._id,
      value: true
    }));
  }, [currentCourse, currentUser, dispatch])

  const {
    task: activeTask,
  } = useMemo(() => {
    if(currentUser && currentCourse) { return getActiveTask(currentUser, currentCourse) }

    return {
      level: null,
      task: null,
      levelProgress: 0
    };
  }, [currentCourse, currentUser])

  const courseWithStatuses = useMemo(() => {
    let isDone = true;
    return {
      ...currentCourse,
      levels: currentCourse?.levels?.map((level) => ({
        ...level,
        isDone,
        tasks: level.tasks?.map((task) => {
          let isActive = false;
          if(task._id === activeTask?._id) {isDone = false; isActive = true;}
          return {
            ...task,
            isDone,
            isActive
          }
        })
      }))
    };
  }, [currentCourse, activeTask])

  const handleOpen = useCallback(() => {
    redirectTo(`/panel/task/${activeTask?._id}`)
  }, [activeTask])

  return (
    <div className={classNames(styles.panel, classes?.main, {
      [styles.panel__hide]: !currentCourse
    })}>
      {showTitle && <Typo variant='title-24'>{
        isTeacher || !activeTask ? 'Структура курса' : 'Мой прогресс'
      }</Typo>}
      <div className={classNames(classes?.list, styles.panel__list)}>
        {courseWithStatuses?.levels?.map((level: any, index: number) => {
          return (
            <div key={level._id} className={classNames(styles.panel__level, {
              [styles['panel__level--active']]: level.isDone,
              [styles['panel__level--disabled']]: !activeTask
            })}>
              <span>
                {showNames ? level.name : `Уровень ${index}`}
                {!isTeacher && !!activeTask && !level.isDone && (
                  <Image
                    src='/images/icons/lock.svg'
                    alt=''
                    className={styles.panel__levelLock}
                  />
                )}
              </span>
              <div>
                {
                  level.tasks?.map((task: any, index: number) => {
                    return (
                      <div
                        key={task._id}
                        className={classNames(styles.panel__task, {
                          [styles['panel__task--active']]: task.isDone,
                          [styles['panel__task--current']]: task.isActive
                        })}
                        onClick={() => redirectTo(`/panel/task/${task._id}`)}
                      >
                        {showNames ? task.name : `Задание ${index + 1}`}
                      </div>
                    )
                  })
                }
              </div>
            </div>
          )})
        }
      </div>
      {showAction && !isTeacher && !!activeTask && currentCourse && <Button variant='primary' onClick={handleOpen}>Продолжить обучение</Button>}
      {showAction && !isTeacher && !activeTask && currentCourse && <Button variant='primary' onClick={handleAssign}>Записаться на курс</Button>}
    </div>
  )
}

export default ProgressDetails;