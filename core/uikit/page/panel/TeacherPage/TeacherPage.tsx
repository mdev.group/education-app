import classNames from "classnames";
import { useForm } from "core/hooks/useForm";
import { getOwnedGroups } from "core/redux/actions/group";
import { beginUpdateUser } from "core/redux/actions/user";
import { currentUser_selector } from "core/redux/selectors";
import { IUserDTO } from "core/redux/types/dto/user";
import Button from "core/uikit/Button";
import Image from "core/uikit/Image";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import Typo from "core/uikit/Typo";
import { declOfNum } from "core/utils/devlOfNum";
import moment from "moment";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import GroupStudents from "./components/GroupStudents/GroupStudents";
import NotificationsPanel from "./components/NotificationsPanel/NotificationsPanel";
import ProgressStudents from "./components/ProgressStudents/ProgressStudents";

import styles from './TeacherPage.module.scss';

const TeacherPage: React.FC = () => {
  const dispatch = useDispatch();

  const [modalOpened, setModalOpened] = useState(false);

  const user = useSelector(currentUser_selector) as IUserDTO;
  
  const userName = useMemo(() => [user?.first_name, user?.last_name].join(' '), [user])
  const userAge = useMemo(() => moment().diff(user?.birth_date, 'years'), [user]);

  const handleDeletePhoto = useCallback(() => {
    dispatch(beginUpdateUser({
      photo_url: null,
    }))
  }, [dispatch])


  const onSubmit = useCallback((formData) => {
    const [first_name, last_name] = formData.name.split(' ');

    dispatch(beginUpdateUser({
      photo_url: formData.photo_url,
      first_name: first_name,
      last_name: last_name,
      email: formData.email,
      birth_date: formData.birth_date
    }))

    setModalOpened(false);
  }, [dispatch])
  
  const {
    formData,
    onChangeField,
    handleSubmit,
    reinit
  } = useForm(onSubmit, {
    ...user,
    name: [user?.first_name, user?.last_name].join(' '),
  })

  useEffect(() => {
    reinit();
  }, [user])

  useEffect(() => {
    dispatch(getOwnedGroups())
  }, [dispatch])

  return (
    <>
      {modalOpened && (
        <ModalContainer
          onClose={() => setModalOpened(false)}
          className={styles.page__modal}
        >
          <ModalForm className={styles.page__modalForm}>
            <Typo variant='title-20'>Настройка аккаунта</Typo>
            <ImageUploader
              label={(
                <>
                  <span className={styles['page__text--green']}>Выберите фото</span>
                  <span>{' '}или перетащите в эту область</span>
                </>
              )}
              onUpload={onChangeField('photo_url')}
              value={formData['photo_url']}
            />
            <Input
              label="Имя фамилия"
              value={formData['name']}
              onChange={onChangeField('name')}
            />
            <Input
              label="e-mail"
              value={formData['email']}
              onChange={onChangeField('email')}
            />
            <Input
              label="Дата рождения"
              type="date"
              value={formData['birth_date']}
              onChange={onChangeField('birth_date')}
            />
            <div className={styles.page__modalActions}>
              <Button
                variant='primary'
                onClick={handleSubmit}
              >
                Сохранить
              </Button>
              <Button
                variant='secondary-border'
                onClick={() => {reinit(); setModalOpened(false)}}
              >
                Отмена
              </Button>
            </div>
          </ModalForm>
        </ModalContainer>
      )}
      <div className={styles.page__container}>
        <div className={styles.page__row}>
          <div className={styles.page__baseInfoWrapper}>
            <div className={styles.page__userPhotoWrapper}>
              <Image
                className={styles.page__userPhoto}
                src={user?.photo_url || "/images/icons/blank/user.svg"}
                alt=""
              />
              <div className={styles.page__userPhotoOverlay}>
                <Button
                  onClick={() => setModalOpened(true)}
                  variant='transparent'
                  className={styles.page__photoAction}
                >
                  <Image
                    src="/images/icons/pen.svg"
                    alt="edit"
                    className={styles.page__photoActionIcon}
                  />
                </Button>

                <Button
                  onClick={handleDeletePhoto}
                  variant='transparent'
                  className={styles.page__photoAction}
                >
                  <Image
                    src="/images/icons/trash.svg"
                    alt="delete"
                    className={styles.page__photoActionIcon}
                  />
                </Button>
              </div>
            </div>
            <div className={styles.page__baseInfo}>
              <Typo variant='title-20'>{userName}</Typo>
              <Typo variant='title-14'>Почта: {user?.email}</Typo>
              {userAge > 0 && <Typo variant='title-14'>Возраст: {userAge} {declOfNum(userAge, ['Год', 'Года', 'Лет'])}</Typo>}
              <Typo variant='title-14'>Статус: Учитель</Typo>
            </div>
          </div>

          <div className={styles.page__notifWrapper}>
            <Typo variant='title-20'>Уведомления</Typo>
            <NotificationsPanel/>
          </div>

          <div className={styles.page__actions}>
            <Button
              onClick={() => setModalOpened(true)}
              variant='transparent'
              className={styles.page__action}
            >
              <Image
                src="/images/icons/pen.svg"
                alt="edit"
              />
            </Button>
          </div>
        </div>

        <div className={styles.page__row}>
          <div className={styles.page__cardWrapper}>
            <Typo variant='title-20'>Прогресс учеников</Typo>
            <div className={styles.page__studentsProgressCard}>
              <ProgressStudents/>
            </div>
          </div>

          <div className={styles.page__cardWrapper}>
            <div className={styles.page__studentsGroupCard}>
              <GroupStudents/>
            </div>
          </div>

          <div className={classNames(styles.page__cardWrapper, styles.page__imageWrapper)}>
            <Image
              src="/images/purr_1.png"
              alt=""
              className={styles.page__purrImage}
            />
          </div>
        </div>
      </div>
    </>
  )
};

export default TeacherPage;