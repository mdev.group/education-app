import { notifications_selector } from 'core/redux/selectors';
import Link from 'next/link';
import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import styles from './NotificationsPanel.module.scss';

const NotificationsPanel:React.FC = () => {
  const notifs = useSelector(notifications_selector);

  const notifsLimited = useMemo(() => notifs?.filter((_, id) => id <= 2 ) || [], [notifs])

  return (
    <div className={styles.panel}>
      {notifsLimited.length > 0 && (
        notifsLimited.map((item, index) => (
          <div className={styles.panel__notify} key={index}>
            {
              item.data.map((dataItem, index) => (
                dataItem.url ? (
                  <Link key={index} href={dataItem.url}>{dataItem.text}</Link>
                ) : (
                  <span key={index}>{dataItem.text}</span>
                )
              ))
            }
          </div>
        ))
      )}
    </div>
  )
}

export default NotificationsPanel;