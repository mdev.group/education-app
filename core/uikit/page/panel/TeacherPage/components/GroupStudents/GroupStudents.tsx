import classNames from "classnames";
import { useForm } from "core/hooks/useForm";
import { beginSearchCourse } from "core/redux/actions/course";
import { beginCreateGroup, beginDeleteGroup, selectGroup } from "core/redux/actions/group";
import { beginAssignPupil, beginSearchPupil } from "core/redux/actions/pupil";
import { ownedGroups_selector, searchedCourses_selector, searchedPupils_selector, selectedGroup_selector } from "core/redux/selectors";
import { IGroupDTO } from "core/redux/types/dto/group";
import { IPupil } from "core/redux/types/dto/pupil";
import Button from "core/uikit/Button";
import Image from "core/uikit/Image";
import Input from "core/uikit/Input/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import SearchInput from "core/uikit/SearchInput/SearchInput";
import Select from "core/uikit/Select";
import Typo from "core/uikit/Typo";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from './GroupStudents.module.scss';

interface IProps {
  onGroupSelect?: (value: string) => void;
}

const GroupStudents: React.FC<IProps> = () => {
  const dispatch = useDispatch();
  
  const groups = useSelector(ownedGroups_selector)
  const selectedGroup = useSelector(selectedGroup_selector)
  const searchedPupils = useSelector(searchedPupils_selector)
  const searchedCourses = useSelector(searchedCourses_selector)

  const isEmpty = useMemo(() => !groups || groups?.length == 0, [groups])
  const isNoStudents = useMemo(() => !selectedGroup || selectedGroup.students.length === 0, [selectedGroup])

  const [showCreateGroupModal, setShowCreateGroupMModal] = useState(false);
  const [showStudentsModal, setShowStudentsMModal] = useState(false);

  const handleOpenCreateGroupForm = useCallback(() => {
    setShowCreateGroupMModal(true);
  }, [])

  const handleCloseCreateGroupForm = useCallback(() => {
    setShowCreateGroupMModal(false);
  }, [])
  
  const onSubmit = useCallback((formData) => {
    dispatch(beginCreateGroup({
      data: {
        name: formData.name,
        courseID: formData.course._id
      }
    }))
    setShowCreateGroupMModal(false);
  }, [dispatch])

  const {
    formData,
    onChangeField,
    handleSubmit,
    reinit
  } = useForm(onSubmit)

  const handleCloseStudentsForm = useCallback(() => {
    setShowStudentsMModal(false)
  }, [])

  const onSubmitStudents = useCallback((formData) => {
    formData.students.forEach((student: IPupil) => {
      dispatch(beginAssignPupil({
        id: student._id,
        groupID: selectedGroup?._id
      }))
    })

    handleCloseStudentsForm();
  }, [dispatch, selectedGroup, handleCloseStudentsForm])

  const {
    formData: stFormData,
    onChangeField: stOnChangeField,
    handleSubmit: stHandleSubmit,
    reinit: stReinit
  } = useForm(onSubmitStudents, {
    students: selectedGroup?.students 
})

  const onGroupSelect = useCallback((item: IGroupDTO) => () => {
    dispatch(selectGroup(item._id))
  }, [dispatch])

  const handleUnassignStudent = useCallback((student: IPupil) => () => {
    dispatch(beginAssignPupil({
      id: student._id,
      groupID: null
    }))
  }, [dispatch])

  const handleDeleteGroup = useCallback(() => {
    dispatch(beginDeleteGroup({
      id: selectedGroup?._id
    }))
  }, [dispatch, selectedGroup])

  const handleOpenStudentsForm = useCallback(() => {
    setShowStudentsMModal(true)
  }, [])

  const handleUpdateStudentSearch = useCallback((username) => {
    dispatch(beginSearchPupil(username))
  }, [dispatch])

  const handleUpdateCourseSearch = useCallback((name) => {
    dispatch(beginSearchCourse(name))
  }, [dispatch])


  const handleAddPupil = useCallback((pupil: IPupil) => () => {
    if(!stFormData.students.some((item:IPupil) => item.username === pupil.username)) {
      const newArr = [...stFormData.students];
      newArr.push(pupil)
      stOnChangeField('students')(newArr as any)
    }
  }, [stOnChangeField, stFormData])

  useEffect(() => {
    stReinit()
  }, [selectedGroup])

  return (
    <>
      {showCreateGroupModal && (
        <ModalContainer
          onClose={handleCloseCreateGroupForm}
          className={styles.panel__modal}
        >
          <Typo variant='title-20'>Создание новой группы</Typo>
          <ModalForm className={styles.panel__modalForm}>
            <Input
              label="Название группы"
              value={formData['name']}
              onChange={onChangeField('name')}
              placeholder="Введите название группы"
            />

            {formData['course'] ? (
              <div>
                <Typo variant='rubrik' className={styles.panel__modalLabel}>Курс</Typo>
                <div onClick={() => onChangeField('course')(undefined as any)} className={styles.panel__selectedCourse}><Typo variant='title-14'>{formData['course'].name}</Typo></div>
              </div>
            ) : (
              <SearchInput
                label="Курс"
                items={searchedCourses?.map((course) => ({
                  label: course.name,
                  onClick: () => onChangeField('course')(course as any)
                })) || []}
                placeholder="Поиск курса"
                onUpdateSearch={handleUpdateCourseSearch}
                failMsg="Курс не найден 😔"
              />
            )}

            <div className={styles.panel__modalFormActions}>
              <Button
                variant='primary'
                onClick={handleSubmit}
              >
                Создать
              </Button>
            </div>
          </ModalForm>
        </ModalContainer>
      )}

      {showStudentsModal && (
        <ModalContainer
          onClose={handleCloseStudentsForm}
          className={styles.panel__modal}
        >
          <Typo variant='title-20'>Добавление новых учеников</Typo>
          <ModalForm className={styles.panel__modalForm}>
            <SearchInput
              placeholder="Логин ученика"
              items={searchedPupils?.map((pupil) => ({
                onClick: handleAddPupil(pupil),
                label: `${[pupil.first_name, pupil.last_name].join(' ')} (${pupil.username})`
              })) || []}
              onUpdateSearch={handleUpdateStudentSearch}
              failMsg="Ученик не найден 😔"
            />

            <div className={styles.panel__modalFormListWrapper}>
              <Typo variant='title-16'>
                {stFormData['students']?.length > 0 ? "Ученики группы" : "В группе нет учеников"}
              </Typo>
              <div className={styles.panel__modalFormList}>
                {stFormData['students']?.map((student: IPupil) => (
                  <div key={student._id} className={styles.panel__modalFormListItem}>
                    <Typo variant='title-14'>{[student.first_name, student.last_name].join(' ')}</Typo>
                    <Typo variant='title-12'>{student.username}</Typo>
                  </div>
                ))}
              </div>
            </div>

            <div className={styles.panel__modalFormActions}>
              <Button
                variant='primary'
                onClick={stHandleSubmit}
              >
                Сохранить
              </Button>
              <Button
                variant='secondary-border'
                onClick={handleCloseStudentsForm}
              >
                Отменить
              </Button>
            </div>
          </ModalForm>
        </ModalContainer>
      )}

      <div className={classNames(styles.panel__container, {
        [styles['panel__container--empty']]: isEmpty || isNoStudents
      })}>
        {(groups && !isEmpty) ? (
          <>
            {isNoStudents ? (
              <div className={styles.panel__placeholder}>
                <div className={styles.panel__placeholderActions}>
                  <Select
                    placeholder="Выберите группу"
                    value={selectedGroup?.name}
                    items={
                      groups.map((item) => ({
                        label: item.name,
                        onClick: onGroupSelect(item)
                      }))
                    }
                  />
                  <Button variant='secondary-border' className={styles.panel__addButton} onClick={handleOpenCreateGroupForm}>
                    <Image
                      src="/images/icons/plus.svg"
                      alt=""
                    />
                  </Button>
                </div>
                <Image
                  src="/images/arrow-2.png"
                  alt=""
                  className={styles.panel__placeholderImage_2}
                />
                <Typo variant='title-16-b'>Добавьте участников, чтобы начать учиться!</Typo>
                <Typo variant='title-14'>Здесь вы сможете создавать новые группы, добавлять и удалять учеников, редактировать и просматривать информацию о группе и учащихся в ней.</Typo>
                <Image
                  src="/images/fishes.png"
                  alt=""
                  className={styles.panel__placeholderAppendImage}
                />

                <div className={styles.panel__actions}>
                  <Button variant='primary' onClick={handleOpenStudentsForm}>
                    Новый участник
                  </Button>

                  <Button variant='secondary-border' onClick={handleDeleteGroup}>
                    Удалить группу
                  </Button>
                </div>
              </div>
            ) : (
              <div className={styles.panel__content}>
                <div className={styles.panel__actions}>
                  <Select
                    placeholder="Выберите группу"
                    value={selectedGroup?.name}
                    items={
                      groups.map((item) => ({
                        label: item.name,
                        onClick: onGroupSelect(item)
                      }))
                    }
                  />
                  <Button variant='secondary-border' className={styles.panel__addButton} onClick={handleOpenCreateGroupForm}>
                    <Image
                      src="/images/icons/plus.svg"
                      alt=""
                    />
                  </Button>
                </div>

                <div className={styles.panel__students}>
                  {selectedGroup?.students.map((student) => (
                    <div key={student._id} className={styles.panel__student}>
                      <Image
                        src={student.photo_url || '/images/icons/blank/user.svg'}
                        alt=""
                        className={styles.panel__studentImage}
                      />
                      <div className={styles.panel__studentInfo}>
                        <Typo variant='title-14'>{[student.first_name, student.last_name].join(' ')}</Typo>
                        <Typo variant='title-12'>{[student.username]}</Typo>
                      </div>
                      <div className={styles.panel__studentActions}>
                        <Button variant='transparent' onClick={handleUnassignStudent(student)}>
                          <Image
                            src="/images/icons/trash-red.svg"
                            alt=""
                          />
                        </Button>
                      </div>
                    </div>
                  ))}
                </div>

                <div className={styles.panel__actions}>
                  <Button variant='primary' onClick={handleOpenStudentsForm}>
                    Новый участник
                  </Button>

                  <Button variant='secondary-border' onClick={handleDeleteGroup}>
                    Удалить группу
                  </Button>
                </div>
              </div>
            )}
          </>
        ) : (
          <div className={styles.panel__placeholder}>
            <div className={styles.panel__placeholderActions}>
              <Select placeholder="Новая группа" items={[]}/>
              <Button variant='secondary-border' className={styles.panel__addButton} onClick={handleOpenCreateGroupForm}>
                <Image
                  src="/images/icons/plus.svg"
                  alt=""
                />
              </Button>
            </div>
            <Image
              src="/images/arrow.png"
              alt=""
              className={styles.panel__placeholderImage}
            />
            <Typo variant='title-16-b'>Начнем с создания новой группы!</Typo>
            <Typo variant='title-14'>Здесь вы сможете создавать новые группы, добавлять и удалять учеников, редактировать и просматривать информацию о группе и учащихся в ней.</Typo>
            <Image
              src="/images/fishes.png"
              alt=""
              className={styles.panel__placeholderAppendImage}
            />
          </div>
        )}
      </div>
    </>
  )
};

export default GroupStudents;