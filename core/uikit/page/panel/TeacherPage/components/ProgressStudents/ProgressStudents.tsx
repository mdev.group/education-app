import classNames from "classnames";
import { beginGetGlobalCourses } from "core/redux/actions/course";
import { currentCourse_selector, globalCourses_selector, myGroup_selector, selectedGroup_selector } from "core/redux/selectors";
import Image from "core/uikit/Image";
import Typo from "core/uikit/Typo";
import { getScoresByUser, calculeteScore } from "core/utils/getScoresByTaskID";
import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from './ProgressStudents.module.scss';

const ProgressStudents: React.FC = () => {
  const dispatch = useDispatch();
  const currentGroup = useSelector(selectedGroup_selector);
  const courses = useSelector(globalCourses_selector);
  const myGroup = useSelector(myGroup_selector);
  const currentCourse = useSelector(currentCourse_selector);

  useEffect(() => {
    dispatch(beginGetGlobalCourses())
  }, [dispatch, currentGroup, myGroup])

  const group = useMemo(() => myGroup || currentGroup, [myGroup, currentGroup])
  const students = useMemo(() => {
    return group?.students.sort((a, b) => {
      const {
        scores: scoreA
      } = getScoresByUser(a, courses);
      const {
        scores: scoreB
      } = getScoresByUser(b, courses)
      return scoreB - scoreA
    })
  }, [group, courses]);

  return (
    <div className={classNames(styles.panel, {
      [styles.panel__placeholderBack]: !group || group?.students.length == 0
    })}>
      {currentCourse && students && students.length > 0 ? students.map((student, index) => {
        const {
          scores
        } = getScoresByUser(student, courses)
        const {
          currentLevel,
        } = calculeteScore(scores)
        const number = index + 1;

        return (
          <div key={student._id} className={styles.panel__student}>
            <Typo variant='title-16'>
              {number}
            </Typo>
            <Image
              className={styles.panel__studentImage}
              src={student.photo_url || '/images/icons/blank/user.svg'}
              alt=""
            />
            <div className={styles.panel__studentInfo}>
              <Typo variant='title-14'>{[student.first_name, student.last_name].join(' ')}</Typo>
              <Typo variant='title-12'>Баллы: {scores}</Typo>
            </div>
            <div className={styles.panel__studentLabel}>Уровень {currentLevel}</div>
            {number === 1 && (
              <div className={styles.panel__studentMedal}>
                <Image
                  src="/images/icons/medal-green.svg"
                  alt=""
                />
              </div>
            )}
          </div>
        )
      }) : (
        <div className={styles.panel__placeholder}>
          <Typo variant='title-16-b'>Пока здесь пусто, но..</Typo>
          <Typo variant='title-14'>После создания группы и добавляния учеников вы сможете отслеживать их успехи в таблице, выявлять отстающих и успешных.</Typo>
        </div>
      )}
    </div>
  )
}

export default ProgressStudents;