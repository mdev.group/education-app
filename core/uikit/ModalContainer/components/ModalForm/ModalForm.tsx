import classNames from "classnames";
import styles from "./ModalForm.module.scss";

interface IProps {
  className?: string;
}

const Userimage: React.FC<IProps> = ({
  children,
  className
}) => { 
  return (
    <div className={classNames(styles.modalForm, className)}>
      {children}
    </div>
  )
}

export default Userimage;