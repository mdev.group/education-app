import classNames from "classnames";
import { ChangeEventHandler, useCallback, useState } from "react";
import Dropdown from "../Dropdown";
import Image from "../Image";
import Input from "../Input/Input";

import styles from './SearchInput.module.scss';

interface IItem {
  label: string;
  onClick: () => void;
}

interface IProps {
  items: IItem[];
  placeholder?: string;
  label?: string;
  onUpdateSearch?: (value: string) => void
  className?: string;
  value?: string;
  failMsg?: string
}

const SearchInput: React.FC<IProps> = ({
  placeholder,
  label,
  onUpdateSearch,
  className,
  items,
  value: valueLabel,
  failMsg = 'Результаты не найдены'
}) => {
  const [value, setValue] = useState('');

  const handleChange = useCallback((newValue) => {
    setValue(newValue);

    if(newValue.length >= 3) {
      if(onUpdateSearch) {
        onUpdateSearch(newValue)
      }
    }
  }, [onUpdateSearch]);

  const handleClick = useCallback((item: IItem) => () => {
    if(item.onClick) item.onClick()
    setValue('')
  }, [])

  return (
    <div className={classNames(className, styles.field)}>
      <Input
        value={valueLabel || value}
        onChange={handleChange}
        placeholder={placeholder}
        label={label}
        className={className}
      />

      <Image
        src="/images/icons/search.svg"
        alt=""
        className={styles.field__icon}
      />
      
      {value.length >= 3 && items && (
        <Dropdown
          classNames={{
            main: styles.field__dropdown
          }}
          items={items.length > 0 ? items.map((item) => (
            {
              onClick: handleClick(item),
              label: item.label
            }
          )) : [
            {
              onClick: () => {},
              label: failMsg,
            }
          ]}
          onClose={() => {}}
        />
      )}
    </div>
  )
};

export default SearchInput;