import classNames from 'classnames';
import { useControlledValue } from 'core/utils/useControlledValue';
import moment from 'moment';
import { ChangeEventHandler, useCallback, useMemo } from 'react';
import Typo from '../Typo';

import styles from './Input.module.scss';

interface IProps {
  value?: any;
  onChange?: (newValue: any) => void;
  label?: string;
  className?: string;
  placeholder?: string;
  isPassword?: boolean;
  type?: string;
  area?: boolean;
  error?: boolean;
}

const Input: React.FC<IProps> = ({
  value: controlledValue,
  onChange,
  label,
  className,
  placeholder,
  isPassword,
  type,
  error,
  area = false
}) => {
  const [value, setValue, isControlled] = useControlledValue(controlledValue)

  const handleChange: ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement> = useCallback(({ currentTarget }) => {
    let newValue: any = currentTarget.value;

    if(type === 'date') {
      newValue = moment(newValue).unix() * 1000
    }


    if(!isControlled) {
      setValue(newValue)
    }
    if (onChange) {
      onChange(newValue);
    }
  }, [onChange, isControlled, setValue, type])

  const formattedValue = useMemo(() => {
    if (type === 'date') {
      return moment(value).format("YYYY-MM-DD")
    }
    return value
  }, [value, type])


  return (
    <label className={classNames(className, styles.field)}>
      {label && <Typo variant='rubrik' className={styles.field__label}>{label}</Typo>}

      {area ? (
        <textarea
          onChange={handleChange}
          className={classNames(styles.field__input, {
            [styles['field__input--error']]: !!error
          })}
          placeholder={placeholder}
          value={formattedValue}
        />
      ) : (
        <input
          value={formattedValue}
          checked={formattedValue}
          onChange={handleChange}
          className={classNames(styles.field__input, {
            [styles['field__input--error']]: !!error
          })}
          placeholder={placeholder}
          type={isPassword ? 'password' : (type || 'text')}
        />
      )}
    </label>
  )
}

export default Input;