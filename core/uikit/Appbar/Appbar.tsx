import classNames from "classnames";
import { appClearAuth } from "core/redux/actions/auth";
import { getUserData } from "core/redux/actions/user";
import { currentUser_selector } from "core/redux/selectors";
import authTokenService from "core/services/authTokenService";
import { isChild } from "core/utils/isChild";
import redirectTo from "core/utils/redirectTo";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button";
import Dropdown from "../Dropdown";
import Image from "../Image";
import Typo from "../Typo";

import styles from './Appbar.module.scss'
import { findTitleByUrl } from "./constants";

const Appbar: React.FC = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const userData = useSelector(currentUser_selector);

  const [activeUser, setActiveUser] = useState(false);

  useEffect(()=>{
    if(!authTokenService.hasTokenInLocalstorage()) {
      redirectTo('/auth');
    }
    dispatch(getUserData());
  }, [dispatch])

  const userName = useMemo(() => [userData?.first_name, userData?.last_name].join(' '), [userData])

  const handleOpen = useCallback((e) => {
    if(isChild(e.target as HTMLElement, e.currentTarget as HTMLElement, 3)) {
      setActiveUser(true)
    }
  }, [])

  const handleUnauth = useCallback(() => {
    authTokenService.clearRefreshData();
    authTokenService.clearToken();
    redirectTo('/auth');
  }, [])

  const pageTitle = useMemo(() => findTitleByUrl(router.asPath), [router.asPath])

  return (
    <div
      className={classNames(styles.appbar, {
        [styles['appbar--active']]: activeUser
      })}
    >
      <h1><Typo variant='title-24'>{pageTitle}</Typo></h1>
      <div>
        <div
          className={styles.appbar__user}
        >
          <div onClick={handleOpen} className={styles.appbar__user}>
            <Typo variant='title-16'>{userName}</Typo>
            <div className={styles.appbar__userImageWrapper}>
              <Image
                className={styles.appbar__userImage}
                src={userData?.photo_url || "/images/icons/blank/user.svg"}
                alt=""
              />
            </div>
            <div className={styles.appbar__chevronBtn}>
              <Image
                src="/images/icons/chevron-bottom.svg"
                alt="open"
                className={styles.appbar__chevron}
              />
            </div>
          </div>
          
          {activeUser && (
            <Dropdown
              items={[
                {
                  label: 'Личный кабинет',
                  onClick: () => {setActiveUser(false); redirectTo('/panel')}
                },
                {
                  label: 'Список курсов',
                  onClick: () => {setActiveUser(false); redirectTo('/panel/courses')}
                },
                {
                  label: 'Выйти',
                  onClick: handleUnauth
                }
              ]}
              onClose={() => setActiveUser(false)}
            />
          )}
        </div>
      </div>
    </div>
  )
}

export default Appbar;