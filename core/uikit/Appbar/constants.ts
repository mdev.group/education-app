const titles = [
  {
    url: '/panel/task',
    title: 'Задачи'
  },
  {
    url: '/panel/courses/',
    title: 'Редактор курса'
  },
  {
    url: '/panel/courses',
    title: 'Курсы'
  },
  {
    url: '/panel',
    title: 'Мой профиль'
  }
]

export const findTitleByUrl = (url: string) => {
  const title = titles.find((item) => url.startsWith(item.url))?.title;

  return title
}