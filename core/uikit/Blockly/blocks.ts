import Blockly from 'blockly';

export const initBlocks = (handleSetModal: any) => {
  window.gameSuccess = () => {
    handleSetModal('success')
  }
  
  window.checkMove = (dir: 'l' | 'r' | 'u' | 'd') => {
    const [x, y] = [window.game.currPoseX, window.game.currPoseY];
    const [ymax, xmax] = [window.gameWin.fields.length, window.gameWin.fields[0].length];

    if(x == 0 && dir == 'l' || x == xmax - 1 && dir == 'r') {
      handleSetModal('error');
      return {
        res: false,
        reason: 'error'
      };
    }

    if(y == 0 && dir == 'u' || y == ymax - 1 && dir == 'd') {
      handleSetModal('error');
      return {
        res: false,
        reason: 'error'
      };
    }

    let nextPosX = x;
    let nextPosY = y;
    if(dir == 'u'){
      nextPosY--
    }
    if(dir == 'd'){
      nextPosY++
    }
    if(dir == 'r'){
      nextPosX++
    }
    if(dir == 'l'){
      nextPosX--
    }

    const nextCeil = window.gameWin.fields[nextPosY][nextPosX];

    if(nextCeil.type == 1) {
      handleSetModal('error');
      return {
        res: false,
        reason: 'error'
      };
    }

    if(nextCeil.type == 5) {
      handleSetModal('error');
      return {
        res: false,
        reason: 'error'
      };
    }

    if(nextCeil.type == 4 && !window.game.mask) {
      handleSetModal('error');
      return {
        res: false,
        reason: 'error'
      };
    }

    return {
      res: true,
    };
  }

  Blockly.Blocks['code_start'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/start.png', 60, 60));
      this.setColour(187);
      this.setNextStatement(true);
    }
  };

  Blockly.Blocks['code_end'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/end.png', 60, 60));
      this.setColour(187);
      this.setPreviousStatement(true);
    }
  };

  Blockly.Blocks['mask'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/mask.png', 180, 80));
      this.setColour(83);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
    }
  };

  Blockly.Blocks['move_right'] = {
    init: function() {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage('/images/blocks/right.svg', 60, 60));
      this.setColour(83);
      this.setNextStatement(true);
      this.setPreviousStatement(true);
    }
  };

  Blockly.Blocks['move_left'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/left.svg', 60, 60));
      this.setColour(83);
      this.setNextStatement(true);
      this.setPreviousStatement(true);
    }
  };


  Blockly.Blocks['move_up'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/up.svg', 60, 60));
      this.setColour(83);
      this.setNextStatement(true);
      this.setPreviousStatement(true);
    }
  };


  Blockly.Blocks['move_down'] = {
    init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage('/images/blocks/down.svg', 60, 60));
      this.setColour(83);
      this.setNextStatement(true);
      this.setPreviousStatement(true);
    }
  };

  Blockly.Blocks['condition'] = {
    init: function() {
      this.setColour(187);
      this.setNextStatement(true);
      this.setPreviousStatement(true);
      this.appendValueInput('IF')
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('direction')
        .appendField('если');
      this.appendStatementInput('DO')
        .appendField('то');
      this.appendStatementInput('ELSE')
        .appendField('иначе');
    }
  };

  Blockly.Blocks['check_right'] = {
    init: function() {
      this.setColour(162);

      this.appendValueInput('WHAT')
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('item')
        .appendField(new Blockly.FieldImage('/images/blocks/check_right.png', 60, 60));

      this.setOutput(true, 'direction');
    }
  };

  Blockly.Blocks['check_left'] = {
    init: function() {
      this.setColour(162);

      this.appendValueInput('WHAT')
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('item')
        .appendField(new Blockly.FieldImage('/images/blocks/check_left.png', 60, 60));

      this.setOutput(true, 'direction');
    }
  };


  Blockly.Blocks['check_top'] = {
    init: function() {
      this.setColour(162);

      this.appendValueInput('WHAT')
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('item')
        .appendField(new Blockly.FieldImage('/images/blocks/check_top.png', 60, 60));

      this.setOutput(true, 'direction');
    }
  };


  Blockly.Blocks['check_bottom'] = {
    init: function() {
      this.setColour(162);

      this.appendValueInput('WHAT')
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('item')
        .appendField(new Blockly.FieldImage('/images/blocks/check_bottom.png', 60, 60));

      this.setOutput(true, 'direction');
    }
  };


  Blockly.Blocks['cactus'] = {
    init: function() {
      this.setColour(158);
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage('/images/blocks/cactus.png', 60, 60));
      this.setOutput(true, 'item');
    }
  };

  Blockly.Blocks['water'] = {
    init: function() {
      this.setColour(158);
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage('/images/blocks/water.svg', 60, 60));
      this.setOutput(true, 'item');
    }
  };


  Blockly.Blocks['repeat'] = {
    init: function() {
      this.appendDummyInput('Times', "Times")
        .appendField(new Blockly.FieldImage('/images/blocks/repeat.svg', 60, 60))
        .appendField(new Blockly.FieldTextInput("3"));
      this.setColour(100);
      this.appendStatementInput('DO')
      this.setNextStatement(true);
      this.setPreviousStatement(true);
    }
  };

  window.gameDelay = (time: number) => {
    return new Promise((resolve) => {
      window.setTimeout(() => {
        resolve(true)
      }, time)
    })
  }

  window.updateCharPos = () => {
    const difX = window.game.currPoseX - window.gameWin.startX;
    const difY = window.game.currPoseY - window.gameWin.startY;
    window.game.character.style.transform = `translateX(calc(${window.game.cellSize} * ${difX})) translateY(calc(${window.game.cellSize} * ${difY}))`;
  }

  Blockly.JavaScript['code_start'] = function(block: any) {
    var code = ``;
    code += `window.game.character.classList.remove('gameChar--mask');\n`;
    code += `await window.gameDelay(500);\n`;
    return code;
  };

  Blockly.JavaScript['code_end'] = function(block: any) {
    var code = `console.log(window.game, window.gameWin);\n`;
    code += `if(+window.game.currPoseX == window.gameWin.posX && +window.game.currPoseY == window.gameWin.posY){\nwindow.gameSuccess()\n}`

    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['repeat'] = function(block: any) {
    const N = Number(block.inputList.find((item: any) => item.name === 'Times').fieldRow[1].value_) || '0'

    const branch = Blockly.JavaScript.statementToCode(block, 'DO')

    var code = `for (let i = 0; i < ${N}; i++) {\n${branch}\n}\n`;

    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['move_right'] = function(block: any) {
    var code = ``;
    code += `if(!window.checkMove('r').res) {return false;};\n`;
    code += `window.game.currPoseX++;\n`;
    code += `window.updateCharPos();\n`;
    code += `await window.gameDelay(1000);\n`;

    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['move_left'] = function(block: any) {
    var code = ``;
    code += `if(!window.checkMove('l').res) {return false;};\n`;
    code += `window.game.currPoseX--;\n`;
    code += `window.updateCharPos();\n`;
    code += `await window.gameDelay(1000);\n`;
    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['move_up'] = function(block: any) {
    var code = ``;
    code += `if(!window.checkMove('u').res) {return false;};\n`;
    code += `window.game.currPoseY--;\n`;
    code += `window.updateCharPos();\n`;
    code += `await window.gameDelay(1000);\n`;
    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['move_down'] = function(block: any) {
    var code = ``;
    code += `if(!window.checkMove('d').res) {return false;};\n`;
    code += `window.game.currPoseY++;\n`;
    code += `window.updateCharPos();\n`;
    code += `await window.gameDelay(1000);\n`;
    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['mask'] = function(block: any) {
    var code = ``;
    code += `window.game.mask = !window.game.mask;\n`;
    code += `if(window.game.mask) {window.game.character.classList.add('gameChar--mask')} else {window.game.character.classList.remove('gameChar--mask')}\n`;
    code += `await window.gameDelay(300);\n`;
    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['condition'] = function(block: any) {
    const cond = Blockly.JavaScript.valueToCode(block, 'IF', Blockly.JavaScript.ORDER_ADDITION) || 'false'

    const branch = Blockly.JavaScript.statementToCode(block, 'DO')
    const branch_2 = Blockly.JavaScript.statementToCode(block, 'ELSE')

    var code = `\nif(${cond}) {\n${branch}\n}\n else \n{\n${branch_2}\n}\n`;

    
    return block.getRootBlock().id == 'start_block' ? code : '';
  };

  Blockly.JavaScript['check_right'] = function(block: any) {
    const checkItemType = Blockly.JavaScript.valueToCode(block, 'WHAT', Blockly.JavaScript.ORDER_ADDITION) || 'null'

    var code = `window.gameWin.fields[window.game.currPoseY] && window.gameWin.fields[window.game.currPoseY][window.game.currPoseX + 1] && window.gameWin.fields[window.game.currPoseY][window.game.currPoseX + 1].type == ${checkItemType}`;

    code = block.getRootBlock().id == 'start_block' ? code : '';

    return [code, Blockly.JavaScript.ORDER_ADDITION];
  };

  Blockly.JavaScript['check_left'] = function(block: any) {
    const checkItemType = Blockly.JavaScript.valueToCode(block, 'WHAT', Blockly.JavaScript.ORDER_ADDITION) || 'null'

    var code = `window.gameWin.fields[window.game.currPoseY] && window.gameWin.fields[window.game.currPoseY][window.game.currPoseX - 1] && window.gameWin.fields[window.game.currPoseY][window.game.currPoseX - 1].type == ${checkItemType}`;

    code = block.getRootBlock().id == 'start_block' ? code : '';

    return [code, Blockly.JavaScript.ORDER_ADDITION];
  };

  Blockly.JavaScript['check_top'] = function(block: any) {
    const checkItemType = Blockly.JavaScript.valueToCode(block, 'WHAT', Blockly.JavaScript.ORDER_ADDITION) || 'null'

    var code = `window.gameWin.fields[window.game.currPoseY-1] && window.gameWin.fields[window.game.currPoseY-1][window.game.currPoseX] && window.gameWin.fields[window.game.currPoseY-1][window.game.currPoseX].type == ${checkItemType}`;

    code = block.getRootBlock().id == 'start_block' ? code : '';

    return [code, Blockly.JavaScript.ORDER_ADDITION];
  };

  Blockly.JavaScript['check_bottom'] = function(block: any) {
    const checkItemType = Blockly.JavaScript.valueToCode(block, 'WHAT', Blockly.JavaScript.ORDER_ADDITION) || 'null'

    var code = `window.gameWin.fields[window.game.currPoseY+1] && window.gameWin.fields[window.game.currPoseY+1][window.game.currPoseX] && window.gameWin.fields[window.game.currPoseY+1][window.game.currPoseX].type == ${checkItemType}`;

    code = block.getRootBlock().id == 'start_block' ? code : '';

    return [code, Blockly.JavaScript.ORDER_ADDITION];
  };

  Blockly.JavaScript['water'] = function(block: any) {
    return ['1', Blockly.JavaScript.ORDER_ADDITION];
  };

  Blockly.JavaScript['cactus'] = function(block: any) {
    return ['5', Blockly.JavaScript.ORDER_ADDITION];
  };
}