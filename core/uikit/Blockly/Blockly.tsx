import Blockly from 'blockly';
import redirectTo from 'core/utils/redirectTo';
import { useCallback, useEffect, useRef, useState } from 'react';
import Button from '../Button';
import Image from '../Image';
import ModalContainer from '../ModalContainer';
import Typo from '../Typo';
import { initBlocks } from './blocks';
import { toolbox } from './constants';

interface IPuzzleAssign {
  count: number;
  type: string;
}

interface IProps {
  handleNextLvl: ()=>void;
  onSuccess: ()=>void;
  puzzles?: IPuzzleAssign[]
  updateToolboxWidth: (width: number)=>void
}

const BlocklyPanel:React.FC<IProps> = ({
  handleNextLvl,
  onSuccess,
  puzzles,
  updateToolboxWidth = () => {}
}) => {
  const workspace = useRef(null);
  const [modalType, setModalType] = useState(null);
  const [toolboxWidth, setToolboxWidth] = useState(50);

  const handleSetModal = useCallback((value) => {
    if(onSuccess && value == 'success') {
      onSuccess();
    }
    setModalType(value)
  }, [onSuccess])

  const handleClear = useCallback(() => {
    handleSetModal(null);
    if(workspace.current) {
      (workspace.current as any).clear();

      var xml =
      '<xml>' +
        '<block id="start_block" x="200" y="50" type="code_start" deletable="false" movable="false"></block>' +
      '</xml>';
      Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(xml), workspace.current);

      if(window.game) {
        window.game.currPoseX = window.gameWin.startX;
        window.game.currPoseY = window.gameWin.startY;
        window.updateCharPos();
      }
    }
  }, [handleSetModal])

  const onNextLvl = useCallback(() => {
    handleClear();
    if(handleNextLvl) handleNextLvl();
  }, [handleNextLvl, handleClear])


  const updatePuzzles = useCallback((event) => {
    if (
      event.type == Blockly.Events.BLOCK_DELETE ||
      event.type == Blockly.Events.BLOCK_CREATE
    ) {
      const currblocks = (workspace.current as any).getAllBlocks();
      const currCounts = currblocks.reduce((prev: any, curr: any) => {
        if(prev[curr.type]) {
          prev[curr.type] += 1;
        } else {
          prev[curr.type] = 1;
        }

        return prev;
      }, {}) as any;

      (workspace.current as any).updateToolbox({
        ...toolbox,
        contents: toolbox.contents.filter((item) => {
          return (!puzzles || puzzles?.length == 0) || item.kind === "sep" || item.type == 'code_end' || puzzles.some((assign) => assign.type === item.type && (assign.count - (currCounts[item.type] || 0)) > 0)
        })
      });

      setToolboxWidth((workspace.current as any).flyout_.width_);
      updateToolboxWidth((workspace.current as any).flyout_.width_);  
    }
  }, [])


  useEffect(() => {
    if(!workspace.current) {
      initBlocks(handleSetModal);
      var xml =
      '<xml>' +
        '<block id="start_block" x="200" y="50" type="code_start" deletable="false" movable="false"></block>' +
      '</xml>';


      workspace.current = Blockly.inject('blocklyDiv', {
        toolbox: {
          ...toolbox,
          contents: toolbox.contents.filter((item) => {
            return puzzles.some((assign) => assign.type === item.type && assign.count > 0)
          })
        },
        move: {
          scrollbars: {
            horizontal: true,
            vertical: true
          },
          drag: true,
          wheel: false
        }
      });
      Blockly.Xml.domToWorkspace(Blockly.Xml.textToDom(xml), workspace.current);
      updateToolboxWidth((workspace.current as any).flyout_.width_);
      setToolboxWidth((workspace.current as any).flyout_.width_);
      (workspace.current as any).addChangeListener(updatePuzzles);
    }
  }, [updateToolboxWidth, handleSetModal, puzzles, updatePuzzles])

  const handlePlay = useCallback(() => {
    if (workspace.current) {
      Blockly.JavaScript.addReservedWords('code');
      window.LoopTrap = 1000;
      Blockly.JavaScript.INFINITE_LOOP_TRAP = 'if(--window.LoopTrap == 0) throw "Infinite loop.";\n';
      var code = Blockly.JavaScript.workspaceToCode(workspace.current);
      try {
        code = `(async ()=>{window.game = { mask: false, currPoseX: window.gameWin.startX, currPoseY: window.gameWin.startY, character: document.getElementById('gameChar'), cellSize: getComputedStyle(document.getElementsByClassName('gameCell')[0]).getPropertyValue('width') };\nwindow.game.character.style.transform = 'none';\n${code}})()`;
        console.log(code);
        
        eval(code);
      } catch (e) {
        alert('Ой... Что-то пошло не так');
      }
    }
  }, [workspace])

  return (
    <>
      {modalType === 'error' && (
        <ModalContainer onClose={() => setModalType(null)} className="blocklyModal">
          <div className='blocklyModalContainer'>
            <Typo variant='title-20'>{`Упс, что-то пошло не так...\nПопробуй еще раз!`}</Typo>
            <Image
              src='/images/blocks/fail.png'
              alt='fail'
            />
            <div className='blocklyModalContainer__actions'>
              <Button variant='primary' onClick={() => {handleSetModal(null); handleClear()}}>
                Попробовать снова
              </Button>
              <Button variant='secondary-border' onClick={() => redirectTo('/panel')}>
                Выйти
              </Button>
            </div>
          </div>
        </ModalContainer>
      )}

      {modalType === 'success' && (
        <ModalContainer onClose={() => setModalType(null)} className="blocklyModal">
          <div className='blocklyModalContainer'>
            <Typo variant='title-20'>{`Победа! Косточка у Вуффа!`}</Typo>
            <Image
              src='/images/blocks/win.png'
              alt='fail'
            />
            <div className='blocklyModalContainer__actions'>
              <Button variant='primary' onClick={() => {handleSetModal(null); onNextLvl()}}>
                Следующий уровень
              </Button>
              <Button variant='secondary-border' onClick={() => redirectTo('/panel')}>
                Выйти
              </Button>
            </div>
          </div>
        </ModalContainer>
      )}

      <div className='blocklyContainer'>
        <div id="blocklyDiv"/>
        <div className="blocklyFooter" style={{width: `calc(100% - ${toolboxWidth}px)`}}>
          <Button variant='transparent' onClick={handleClear}>
            <Image
              src="/images/icons/trash-white.svg"
              alt=""
            />
          </Button>

          <Button variant='transparent' onClick={handlePlay}>
            <Image
              src="/images/icons/play.svg"
              alt=""
            />
          </Button>
        </div>
      </div>
    </>
  )
}

export default BlocklyPanel;