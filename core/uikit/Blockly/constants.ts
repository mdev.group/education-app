
export var toolbox = {
  "kind": "flyoutToolbox",
  "contents": [
    {
      "kind": "block",
      "type": "move_right",
      "name": "Движение вправо"
    },
    {
      "kind": "block",
      "type": "move_left",
      "name": "Движение влево"
    },
    {
      "kind": "block",
      "type": "move_up",
      "name": "Движение вверх"
    },
    {
      "kind": "block",
      "type": "move_down",
      "name": "Движение вниз"
    },
    {
      "kind": "block",
      "type": "repeat",
      "name": "Повторить"
    },
    {
      "kind": "block",
      "type": "mask",
      "name": "Одеть маску"
    },
    {
      "kind": "block",
      "type": "code_end",
      "name": "Финиш"
    },
    {
      "kind": "block",
      "type": "condition",
      "name": "Условие"
    },
    {
      "kind": "block",
      "type": "check_right",
      "name": "Проверить справа"
    },
    {
      "kind": "block",
      "type": "check_top",
      "name": "Проверить сверху"
    },
    {
      "kind": "block",
      "type": "check_left",
      "name": "Проверить слева"
    },
    {
      "kind": "block",
      "type": "check_bottom",
      "name": "Проверить снизу"
    },
    {
      "kind": "block",
      "type": "cactus",
      "name": "Кактус"
    },
    {
      "kind": "block",
      "type": "water",
      "name": "Вода"
    }
  ]
}