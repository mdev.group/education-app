import classNames from 'classnames';
import { TFunc } from 'core/types/types';
import { useCallback, useEffect, useRef } from 'react';
import styles from './Image.module.scss';

/* eslint-disable @next/next/no-img-element */
interface IProps {
  src: string;
  alt?: string;
  className?: string;
  placeholderUrl?: string;
  onClick?: TFunc;
}

const Image: React.FC<IProps> = ({
  src,
  alt = "",
  className,
  placeholderUrl = '',
  onClick
})=>{
  const ref = useRef<HTMLImageElement>(null);

  const onError = useCallback(({currentTarget}) => {
      currentTarget.src = placeholderUrl;
  }, [placeholderUrl])

  useEffect(() => {
    if(ref.current && ref.current.src !== src) {
      ref.current.src = src;
    }
  }, [src, ref])

  return (
    <img ref={ref} src={src} alt={alt} className={classNames(
      className,
      styles.image
    )} onError={onError} onClick={onClick}/>
  )
}

export default Image;