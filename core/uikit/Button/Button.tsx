import React from 'react';
import cn from 'classnames';

import styles from './Button.module.scss';
import { TFunc } from 'core/types/types';

type size = 'sm' | 'md' | 'lg';
type variant = 'primary' | 'secondary' | 'secondary-border' | 'transparent';

interface IButtonProps {
  className?: string;
  type?: 'submit' | 'reset' | 'button' | undefined;

  onClick?: TFunc;
  disabled?: boolean;

  size?: size;
  variant: variant;
}

const Button: React.FC<IButtonProps> = ({
  children,
  size = 'md',
  className,
  variant = 'filled',
  type = 'button',
  onClick,
  disabled
}) => {
  return <button
    type={type}
    className={cn(
      [
        styles.Button,
        className
      ],
      {
        [styles[`Button--size-${size}`]]: !!size,
        [styles[`Button--variant-${variant}`]]: !!variant,
        [styles['Button--disabled']]: disabled
      }
    )}
    onClick={!disabled ? onClick : undefined}
    disabled={disabled}
  >
    <div className={styles.Button__container}>
      {children}
    </div>
  </button>
}

export default Button;