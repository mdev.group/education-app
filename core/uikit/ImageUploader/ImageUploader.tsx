import classNames from "classnames";
import { host } from "core/api/request";
import { ChangeEventHandler, useCallback, useEffect } from "react";
import Image from "../Image/Image"

import styles from './ImageUploader.module.scss'

interface IProps {
  value?: string;
  onUpload?: (newValue: string) => void;
  className?: string;
  label?: React.ReactNode;
}

const ImageUploader: React.FC<IProps> = ({
  value,
  onUpload,
  className,
  label
}) => {
  const handleUpload: ChangeEventHandler<HTMLInputElement> = useCallback(async (e) => {
    if(e.target.files) {
      const formData = new FormData();

      formData.append('file', e.target.files[0])

      const res = await fetch(`https://api.m-dev.group/image`, {
        method: 'POST',
        body: formData
      })

      const data = await res.json()

      if(onUpload) onUpload(data.url);
    }
  }, [onUpload])

  return (
    <label className={classNames(className, styles.uploader)}>
      <input type='file' onChange={handleUpload} className={styles.uploader__input}/>

      {!value ? (
        <div className={styles.uploader__field}>
          <span>{label}</span>
        </div>
      ) : (
        <div  className={styles.uploader__field}>
          <div className={styles.uploader__imageField}>
            <Image
              src={value}
              alt=""
            />
          </div>
          <span>{label}</span>
        </div>
      )}
    </label>
  )
}

export default ImageUploader;