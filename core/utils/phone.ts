export const formatPhone = (value: string): string => {
  try {
      return value.replace(/^\+7(\d{3})(\d{3})(\d{2})(\d{2}).*/, '+7 ($1) $2-$3-$4') || value;
  } catch (e) {
      return value || '';
  }
};

/**
* Преобразует строковое значение номера телефона из разных форматов к единому
* Примеры:
* 89001112233 -> +79001112233
* 8 900 111 22 33 -> +79001112233
* 7(900)111-22-33 -> +79001112233
* 9001112233 -> +79001112233
*
* @param value Телефон строковым значением
*
* @returns string если в номере телефона более 10 цифр, то возвращается шаблонное значение,
* иначе то возвращается исходная строка
*/

export const formatPhoneNumber = (value: string): string => {
  try {
      const lastNumbers = value.match(/\d/g).join('');
      return lastNumbers?.length > 9 ? `+7${lastNumbers.substr(lastNumbers.length - 10)}` : value;
  } catch (e) {
      return value || '';
  }
};

export const validatePhone = (phone: string, usePrefix?: boolean): boolean => (
  (usePrefix ? /^\+7(\d{10})/ : /^\d{10}$/).test(phone || '')
);

export const deletePhonePrefix = (phone: string): string => (
  phone.replace(/^\+7/, '')
);

export const addPhonePrefix = (phone: string): string => (`+7${phone}`);