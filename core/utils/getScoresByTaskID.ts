import { ICourse } from "core/redux/types/dto/course";
import { ILevelDTO } from "core/redux/types/dto/level";
import { IPupil } from "core/redux/types/dto/pupil";
import { ITaskDTO } from "core/redux/types/dto/task";

export const calculeteScore = (score: number) => {
  const totalMin = 10;
  if(score < totalMin) {
    return {
      currentLevel: 0,
      lvlMin: 0,
      lvlMax: totalMin,
      progress: score / totalMin,
      score
    }
  }

  const currentLevel = Math.floor(Math.sqrt((score - 6) / 4));

  const lvlMin = Math.floor(Math.pow(currentLevel, 2) * 4 + 6);
  const nextLvlMin = Math.floor(Math.pow((currentLevel + 1), 2) * 4 + 6);

  const progress = (score - lvlMin) / (nextLvlMin - lvlMin);

  return {
    currentLevel,
    lvlMin,
    nextLvlMin,
    progress,
    score
  }
}

export const getScoresByUser = (user: IPupil, ownedCourses: ICourse[]) => {
  const res = {
    scores: 0,
    doneTasks: 0
  }

  user.courses?.filter((assignement) => !!assignement.taskID).forEach((assignement) => {
    const course = ownedCourses.find((item) => item._id === assignement.ID);
    console.log(`[${course?.name}]`)

    if(course && course.levels) {
      let prevTaskIsDone = true;
      course.levels.forEach((level) => {
        const allTasksDone = level.tasks?.every((task) => {
          if(!prevTaskIsDone) return false;
    
          console.log(`[${task.name}] +10`)
          res.scores += 10;
          res.doneTasks += 1;

          const isEndDone = task._id === assignement.taskID;
          prevTaskIsDone = !isEndDone;

          return true;
        })
    
        if(!allTasksDone) return;
    
        console.log(`[${level.name}] +20`)
        res.scores += 20;
      })
    }
  })

  return res;
}

export const getStudentTaskIDByCourse = (pupil: IPupil, courseID: string) => {
  return pupil.courses?.find((item) => item.ID === courseID)?.taskID || null;
}

export const getCurrentLevel = (pupil: IPupil, course: ICourse) => {
  const courseAssign = pupil.courses?.find((item) => item.ID === course?._id)

  if(course && courseAssign?.taskID === null) {
    return course.levels ? course.levels[0] : null;
  }

  return (course && course.levels) ? course.levels.find((item) => item.tasks?.some((task) => task._id === courseAssign?.taskID)) || null : null;
}


export const getActiveTask = (pupil: IPupil, course: ICourse) => {
  const courseAssign = pupil.courses?.find((item) => item.ID === course?._id)

  if(!courseAssign) {
    return {
      task: null,
      level: null,
      levelProgress: 0
    }
  }

  if(courseAssign?.taskID === null) {
    const levels = course.levels && course.levels || [];

    return {
      task: levels[0] ? (levels[0].tasks![0] || null) : null,
      level: levels[0] || null,
      levelProgress: 0
    }
  }


  let activeTask: ITaskDTO | null = null;
  let activeLevel: ILevelDTO | null = null;

  let lastTask: ITaskDTO;
  let lastLevel: ILevelDTO;

  let getFlg = false;

  course.levels?.forEach((level) => {
    level.tasks?.forEach((task) => {
      lastLevel = level;
      lastTask = task;

      if (!getFlg && courseAssign?.taskID == task._id) {
        getFlg = true;
        return ;
      }

      if(getFlg && !activeTask && !activeLevel) {
        activeTask = task;
        activeLevel = level;
      }
    })
  })

  const task = activeTask! || lastTask!;
  const level = activeLevel! || lastLevel!;

  return {
    task,
    level,
    levelProgress: (!activeTask) ? 1 : (level.tasks!.findIndex((item) => item._id === task._id)) / level.tasks!.length,
  }
}