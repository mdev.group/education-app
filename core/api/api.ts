import { ICourse } from 'core/redux/types/dto/course';
import { IGroupDTO } from 'core/redux/types/dto/group';
import { ILevelDTO } from 'core/redux/types/dto/level';
import { INotification } from 'core/redux/types/dto/notification';
import { IPupil } from 'core/redux/types/dto/pupil';
import { ITaskDTO } from 'core/redux/types/dto/task';
import { IUserDTO } from 'core/redux/types/dto/user';
import { removeEmptyFields } from 'core/utils/removeEmptyFields';
import getApi from './getApi';
import { employeeFields, generateCafeGetter, generateGetter } from './structures';
import { refreshToken } from './utils';

export enum ERequestTypes {
    GET = 'get',
    POST = 'post',
    PATCH = 'patch',
    DELETE = 'delete'
}

export interface IApi {
    refreshToken: () => any;

    authUser: (data: any) => any;

    registerUser: (data: {
        email: string
    }) => any;

    getUserData: () => IUserDTO;

    updateUser: ({data} : {data: Partial<IUserDTO>}) => IUserDTO;

    ownedGroups: () => IGroupDTO[];
    createGroup: ({data} : {data: Partial<IGroupDTO>}) => IGroupDTO;
    deleteGroup: ({id} : {id: string}) => boolean;
    getGroup: ({id} : {id: string}) => IGroupDTO;

    courseAssignPupil: ({data} : {data: {id: string; courseID: string | null}}) => IPupil;
    assignPupil: ({data} : {data: {id: string; groupID: string | null}}) => IPupil;
    searchPupil: (searchString: string) => IPupil[];

    searchCourse: (searchString: string) => ICourse[];
    getCourse: ({id}: {id: string}) => ICourse;

    getNotifs: () => INotification[];

    getOwnedCourses: () => ICourse[];
    getAssignedCourses: () => ICourse[];
    getGlobalCourses: () => ICourse[];

    solve: ({id}: {id: string}) => any;
    updateCourse: ({id, newData} : {id: string, newData: Partial<ICourse>}) => ICourse;
    updateTask: ({id, newData} : {id: string, newData: Partial<ITaskDTO>}) => ITaskDTO;

    createTask: ({data} : {data: Partial<ITaskDTO>}) => ITaskDTO;
    createLevel: ({data} : {data: Partial<ITaskDTO>}) => ILevelDTO;
    createCourse: ({data} : {data: Partial<ICourse>}) => ICourse;
}

const api: IApi = getApi({
    refreshToken: {
        requestHandler: refreshToken
    },
    authUser: {
        path: () => '/auth/login',
        type: ERequestTypes.POST
    },

    registerUser: {
        path: () => '/auth/register',
        type: ERequestTypes.POST
    },

    getUserData: {
        path: () => '/user/me',
        type: ERequestTypes.GET
    },


    updateUser: {
        path: () => '/user',
        type: ERequestTypes.PATCH,
        dataHandler: ({data}) => data
    },

    ownedGroups: {
        path: () => '/group/owned',
        type: ERequestTypes.GET,
    },

    createGroup: {
        path: () => '/group',
        type: ERequestTypes.POST,
    },

    deleteGroup: {
        path: ({id}) => `/group/${id}`,
        type: ERequestTypes.DELETE,
    },

    assignPupil: {
        path: ({data: {id}}) => `/pupil/${id}/group`,
        dataHandler: ({data: {groupID}}) => ({
            groupID
        }),
        type: ERequestTypes.PATCH,
    },

    searchPupil: {
        path: () => `/pupil/search`,
        dataHandler: (searchString) => ({
            searchString
        }),
        type: ERequestTypes.POST,
    },

    searchCourse: {
        path: () => `/course/search`,
        dataHandler: (searchString) => ({
            searchString
        }),
        type: ERequestTypes.POST,
    },

    getCourse: {
        path: ({id}) => `/course/${id}/structure`,
        dataHandler: () => ({}),
        type: ERequestTypes.GET,
    },

    getNotifs: {
        path: () => `/notification`,
        type: ERequestTypes.GET,
    },

    getGroup: {
        path: ({id}) => `/group/${id}`,
        type: ERequestTypes.GET,
    },

    getOwnedCourses: {
        path: () => `/course/owned`,
        type: ERequestTypes.GET,
    },

    getAssignedCourses: {
        path: () => `/pupil/course/my`,
        type: ERequestTypes.GET,
    },

    getGlobalCourses: {
        path: () => `/course`,
        type: ERequestTypes.GET,
    },

    courseAssignPupil: {
        path: ({data: {id}}) => `/pupil/${id}/course`,
        dataHandler: ({data: {courseID, value}}) => ({
            courseID,
            value
        }),
        type: ERequestTypes.PATCH,
    },

    solve: {
        path: ({id}) => `/task/${id}/solve`,
        dataHandler: () => ({}),
        type: ERequestTypes.POST,
    },

    updateCourse: {
        path: ({id}) => `/course/${id}`,
        dataHandler: ({newData}) => (newData),
        type: ERequestTypes.PATCH,
    },

    updateTask: {
        path: ({id}) => `/task/${id}`,
        dataHandler: ({newData}) => (newData),
        type: ERequestTypes.PATCH,
    },

    createTask: {
        path: () => `/task`,
        dataHandler: ({data}) => (data),
        type: ERequestTypes.POST,
    },

    createLevel: {
        path: () => `/level`,
        dataHandler: ({data}) => (data),
        type: ERequestTypes.POST,
    },

    createCourse: {
        path: () => `/course`,
        dataHandler: ({data}) => (data),
        type: ERequestTypes.POST,
    },
});

export default api;