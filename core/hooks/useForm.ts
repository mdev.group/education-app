import { TFunc } from "core/types/types";
import mergeDeep from "core/utils/mergeDeep";
import React, { FormEventHandler, useCallback, useEffect, useMemo, useState } from "react";
import _ from "lodash";


export function useForm(onSubmit: TFunc, initialValues:any | undefined = {}) {
  const [formData, setFormData] = useState<any>(() => {
    if (!initialValues) {
      return {}
    }

    return initialValues;
  });
  const onChangeField = useCallback((fieldName)=>(newValue: string)=>{
    const currStateClone = JSON.parse(JSON.stringify(formData))
    const fieldStruct: string[] = fieldName.split('.');

    let link = currStateClone;
    fieldStruct.forEach((key: any, index) => {
      const keyIsInt = Number.isInteger(key);
      const finKey = keyIsInt ? Number(key) : key;

      if(index === fieldStruct.length - 1) {
        link[finKey] = newValue
      } else {
        link = link[finKey] || (keyIsInt ? [] : {})
      }
    }, {})

    setFormData(currStateClone);
  }, [formData]);

  const handleSubmit: ()=>void = useCallback((e:any)=>{
    if(e) e.preventDefault();
    if(onSubmit) onSubmit(formData);
  }, [onSubmit, formData]) as any

  const reinit = useCallback(() => {
    setFormData(initialValues || {})
  }, [initialValues])

  const isChanged = useMemo(() => {
    return !_.isEqual(formData, initialValues)
  }, [formData, initialValues])

  return {
    formData,
    onChangeField,
    handleSubmit,
    reinit,
    isChanged
  }
}