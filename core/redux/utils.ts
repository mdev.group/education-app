import { all } from 'redux-saga/effects';

export const createRootSaga = (sagas: Array<() => Generator>) => function* rootSaga() {
    yield all(sagas.map((saga) => saga()));
};
