import { createAction } from "redux-actions";

export const beginAssignPupil = createAction(
  'BEGIN_ASSIGN_PUPIL'
);

export const beginSearchPupil = createAction(
  'BEGIN_SEARCH_PUPIL'
);

export const successSearchPupil = createAction(
  'SUCCESS_SEARCH_PUPIL'
);

export const beginCourseAssignPupil = createAction(
  'BEGIN_COURSE_ASSIGN_PUPIL'
);

export const successCourseAssignPupil = createAction(
  'SUCCESS_COURSE_ASSIGN_PUPIL'
);
