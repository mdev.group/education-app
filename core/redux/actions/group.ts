import { createAction } from "redux-actions";
import { IGroupDTO } from "../types/dto/group";

export const getOwnedGroups = createAction(
  'GET_OWNED_GROUPS'
);

export const setOwnedGroups = createAction<IGroupDTO[]>(
  'SET_OWNED_GROUPS'
);

export const selectGroup = createAction(
  'SELECT_GROUP'
);


export const getMyGroup = createAction(
  'GET_MY_GROUP'
);
export const successGetMyGroup = createAction(
  'SUCCESS__GET_MY_GROUP'
);


export const beginCreateGroup = createAction(
  'BEGIN_CREATE_GROUP'
);

export const beginDeleteGroup = createAction(
  'BEGIN_DELETE_GROUP'
);