import { createAction } from "redux-actions";

export const beginSearchCourse = createAction(
  'BEGIN_SEARCH_COURSE'
);
export const successSearchCourse = createAction(
  'SUCCESS_SEARCH_COURSE'
);

export const beginGetCourse = createAction(
  'BEGIN_GET_CURRENT_COURSE'
);
export const successGetCourse = createAction(
  'SUCCESS_GET_CURRENT_COURSE'
);

export const beginGetGlobalCourses = createAction(
  'BEGIN_GET_GLOBAL_COURSE'
);
export const successGetGlobalCourses = createAction(
  'SUCCESS_GET_GLOBAL_COURSE'
);

export const beginGetOwnedCourses = createAction(
  'BEGIN_GET_OWNED_COURSE'
);
export const successGetOwnedCourses = createAction(
  'SUCCESS_GET_OWNED_COURSE'
);

export const beginSolveTask = createAction(
  'BEGIN_SOLVE_TASK'
);

export const beginUpdateCourse = createAction(
  'BEGIN_UPDATE_COURSE'
);

export const beginUpdateTask = createAction(
  'BEGIN_UPDATE_TASK'
);


export const beginCreateLevel = createAction(
  'BEGIN_CREATE_LEVEL'
);

export const beginCreateTask = createAction(
  'BEGIN_CREATE_TASK'
);

export const beginCreateCourse = createAction(
  'BEGIN_CREATE_COURSE'
);