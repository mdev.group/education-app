import { createAction } from "redux-actions";
import { IPupil } from "../types/dto/pupil";
import type { IUserDTO } from "../types/dto/user";

export const getUserData = createAction(
  'GET_USER_DATA'
);

export const setUserData = createAction<IUserDTO>(
  'SET_USER_DATA'
);

export const beginUpdateUser = createAction<Partial<IUserDTO> | Partial<IPupil>>(
  'BEGIN_UPDATE_USER_DATA'
);

export const beginUpdateNotifs = createAction(
  'BEGIN_UPDATE_USER_NOTIFS'
);

export const SuccessUpdateNotifs = createAction(
  'SUCCESS_UPDATE_USER_NOTIFS'
);