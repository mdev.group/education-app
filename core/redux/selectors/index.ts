import { createSelector } from "@reduxjs/toolkit"
import { IRootReducer } from "../reducers"
import { IPupil } from "../types/dto/pupil";

export const currentUser_selector = (state: IRootReducer) => state.user.userData;
export const ownedGroups_selector = (state: IRootReducer) => state.group.ownedGroups;
export const selectedGroupID_selector = (state: IRootReducer) => state.group.selectedID;
export const searchedPupils_selector = (state: IRootReducer) => state.pupil.searchPupils;
export const searchedCourses_selector = (state: IRootReducer) => state.course.searchCourses;
export const currentCourse_selector = (state: IRootReducer) => state.course.currentCourse;
export const notifications_selector = (state: IRootReducer) => state.user.notifies;
export const myGroup_selector = (state: IRootReducer) => state.group.myGroup;

export const globalCourses_selector = (state: IRootReducer) => state.course.globalCourses || [];
export const ownedCourses_selector = (state: IRootReducer) => state.course.ownedCourses || [];

export const isTeacher_selector = createSelector([currentUser_selector], (userData) => !!(userData as any)?.email)

export const selectedGroup_selector = createSelector([selectedGroupID_selector, ownedGroups_selector], (ID, groups) => groups?.find((item) => item._id === ID))