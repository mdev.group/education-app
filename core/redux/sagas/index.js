import { call, spawn, all } from 'redux-saga/effects'

import { initAuthWatchers } from './authSaga';
import { initCourseWatchers } from './courseSaga';
import { initGroupWatchers } from './groupSaga';
import { initPupilWatchers } from './pupilSaga';
import { initUserWatchers } from './userSaga';

export default function* rootSaga(){
  const sagas = [initAuthWatchers, initUserWatchers, initGroupWatchers, initPupilWatchers, initCourseWatchers];
  
  const retrySagas = sagas.map(saga=>{
    return spawn(function* (){
      while(true){
        try{
          yield call(saga);
          break;
        } catch (e) {
          console.error(e)
        }
      }
    })
  })

  yield all(retrySagas)
}