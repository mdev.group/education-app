import { takeEvery, put, call, select } from 'redux-saga/effects'
import { Action } from 'redux'
import api from '../../api';

import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { beginCreateGroup, beginDeleteGroup, getMyGroup, getOwnedGroups, selectGroup, setOwnedGroups, successGetMyGroup } from '../actions/group';
import { IGroupDTO } from '../types/dto/group';
import { selectedGroupID_selector } from '../selectors';


function* ownedGroups(): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.ownedGroups)) as IGroupDTO[];

    yield put(setOwnedGroups(response));
    const selectedGroupID = yield select(selectedGroupID_selector);
    if(!selectedGroupID || !response.some((gr) => gr._id == selectedGroupID)) {
      yield put(selectGroup(response[0]._id));
    }
  } catch (error) {
      yield put(requestError(error));
  }
}

function* create({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    yield call(api.createGroup, payload.data);

    yield put(getOwnedGroups());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* deleteGroup({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    yield call(api.deleteGroup, payload);

    yield put(getOwnedGroups());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* myGroup({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getGroup, {
      id: payload.id
    })) as IGroupDTO;

    yield put(successGetMyGroup(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

export function* initGroupWatchers(){
  yield takeEvery(getOwnedGroups.toString(), ownedGroups);
  yield takeEvery(getMyGroup.toString(), myGroup);
  yield takeEvery(beginCreateGroup.toString(), create);
  yield takeEvery(beginDeleteGroup.toString(), deleteGroup);
}