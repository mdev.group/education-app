import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import api from '../../api';
import { requestError } from '../actions/common';
import { waitFor, waitForAuth } from './common';
import { successSearchPupil } from '../actions/pupil';
import { IPupil } from '../types/dto/pupil';
import { beginCreateCourse, beginCreateLevel, beginCreateTask, beginGetCourse, beginGetGlobalCourses, beginGetOwnedCourses, beginSearchCourse, beginSolveTask, beginUpdateCourse, beginUpdateTask, successGetCourse, successGetGlobalCourses, successGetOwnedCourses, successSearchCourse } from '../actions/course';
import { ICourse } from '../types/dto/course';
import { selectGroup } from '../actions/group';
import { ownedGroups_selector, selectedGroup_selector } from '../selectors';
import { IGroupDTO } from '../types/dto/group';
import {
  getUserData as getUserDataAction,
} from '../actions/user';

function* search({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.searchCourse, payload)) as ICourse[];

    yield put(successSearchCourse(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getCourse({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getCourse, payload)) as ICourse;

    yield put(successGetCourse(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getGroupCourse({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    yield call(waitFor, ownedGroups_selector);
    const ownedGroups = (yield select(ownedGroups_selector)) as IGroupDTO[];

    yield put(beginGetCourse({
      id: ownedGroups.find((item) => item._id === payload)?.courseID
    }))
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getOwnedCourses({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    let response;
    if (payload.userType === 'student') {
      response = (yield call(api.getAssignedCourses)) as ICourse[];
    } else {
      response = (yield call(api.getOwnedCourses)) as ICourse[];
    }

    yield put(successGetOwnedCourses(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getGlobalCourses({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    const response = (yield call(api.getGlobalCourses)) as ICourse[];

    yield put(successGetGlobalCourses(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* solveTask({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.solve, {
      id: payload.id
    });

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* updateCourse({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.updateCourse, {
      id: payload.id,
      newData: payload.newData
    });

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* updateTask({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.updateTask, {
      id: payload.id,
      newData: payload.newData
    });

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* createLevel({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.createLevel, payload);

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* createTask({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.createTask, payload);

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* createCourse({payload}: any): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.createCourse, payload);

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
    yield put(beginGetGlobalCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}


export function* initCourseWatchers(){
  yield takeEvery(beginSearchCourse.toString(), search);
  yield takeEvery(beginGetCourse.toString(), getCourse);
  yield takeEvery(selectGroup.toString(), getGroupCourse);
  yield takeEvery(beginGetOwnedCourses.toString(), getOwnedCourses);
  yield takeEvery(beginGetGlobalCourses.toString(), getGlobalCourses);
  yield takeEvery(beginSolveTask.toString(), solveTask);
  yield takeEvery(beginUpdateCourse.toString(), updateCourse);
  yield takeEvery(beginUpdateTask.toString(), updateTask);
  yield takeEvery(beginCreateLevel.toString(), createLevel);
  yield takeEvery(beginCreateTask.toString(), createTask);
  yield takeEvery(beginCreateCourse.toString(), createCourse);
}