import redirectTo from 'core/utils/redirectTo';
import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import { Action } from 'redux'
import { v4 as uuidv4 } from 'uuid';
import api from '../../api';
import authTokenService from '../../services/authTokenService';

import {
  beginUpdateUser,
  getUserData as getUserDataAction, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { IUserDTO } from '../types/dto/user';
import { beginAssignPupil, beginCourseAssignPupil, beginSearchPupil, successSearchPupil } from '../actions/pupil';
import { getOwnedGroups } from '../actions/group';
import { IPupil } from '../types/dto/pupil';
import { beginGetOwnedCourses } from '../actions/course';

function* assign({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    yield call(api.assignPupil, {
      data: {
        groupID: payload.groupID,
        id: payload.id
      }
    });

    yield put(getOwnedGroups());
  } catch (error) {
      yield put(requestError(error));
  }
}

function* search({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.searchPupil, payload)) as IPupil[];

    yield put(successSearchPupil(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* courseAssign({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    yield call(api.courseAssignPupil, {
      data: payload
    });

    yield put(getUserDataAction());
    yield put(beginGetOwnedCourses());
  } catch (error) {
      yield put(requestError(error));
  }
}

export function* initPupilWatchers(){
  yield takeEvery(beginAssignPupil.toString(), assign);
  yield takeEvery(beginSearchPupil.toString(), search);
  yield takeEvery(beginCourseAssignPupil.toString(), courseAssign);
}