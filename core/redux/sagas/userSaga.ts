import delay from '@redux-saga/delay-p'
import redirectTo from 'core/utils/redirectTo';
import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import { Action } from 'redux'
import { v4 as uuidv4 } from 'uuid';
import api from '../../api';
import authTokenService from '../../services/authTokenService';

import {
  beginUpdateNotifs,
  beginUpdateUser,
  getUserData as getUserDataAction, setUserData, SuccessUpdateNotifs,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { IUserDTO } from '../types/dto/user';
import { INotification } from '../types/dto/notification';

function* getUserData(): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getUserData)) as IUserDTO;
    yield put(setUserData(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* getNotifications(): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getNotifs)) as INotification[];
    yield put(SuccessUpdateNotifs(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* updateUser({payload}: any): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.updateUser, {
      data: payload
    })) as IUserDTO;

    yield put(setUserData(response));
  } catch (error) {
      yield put(requestError(error));
  }
}

function* fetchSagaPeriodically() {
  console.log('bind notifications check')
  while (true) {
    yield call(getNotifications);
    yield delay(60000);
  }
}

export function* initUserWatchers(){
  yield takeEvery(getUserDataAction.toString(), getUserData);
  yield takeEvery(beginUpdateUser.toString(), updateUser);
  yield takeEvery(beginUpdateNotifs.toString(), getNotifications);

  yield call(fetchSagaPeriodically)
}