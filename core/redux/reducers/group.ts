import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { selectGroup, setOwnedGroups, successGetMyGroup } from '../actions/group';

import { IGroupDTO } from '../types/dto/group';

export interface IGroupState {
  ownedGroups?: IGroupDTO[];
  selectedID?: string;
  myGroup?: IGroupDTO;
}

const initialState: IGroupState = {
  ownedGroups: undefined,
  selectedID: undefined,
  myGroup: undefined
};

// TODO: remove all any
const reducer = handleActions<IGroupState, any>({
  [setOwnedGroups.toString()]: (state, {payload}: Action<IGroupDTO[]>) => ({
    ...state,
    ownedGroups: payload
  }),

  [selectGroup.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    selectedID: payload
  }),

  [successGetMyGroup.toString()]: (state, {payload}: Action<IGroupDTO>) => ({
    ...state,
    myGroup: payload
  })
}, initialState);

export default reducer;
