import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { selectGroup, setOwnedGroups } from '../actions/group';
import { successSearchPupil } from '../actions/pupil';

import { IGroupDTO } from '../types/dto/group';
import { IPupil } from '../types/dto/pupil';

export interface IPupilState {
  searchPupils?: IPupil[];
}

const initialState: IPupilState = {
  searchPupils: undefined,
};

// TODO: remove all any
const reducer = handleActions<IPupilState, any>({
  [successSearchPupil.toString()]: (state, {payload}: Action<IPupil[]>) => ({
    ...state,
    searchPupils: payload
  }),
}, initialState);

export default reducer;
