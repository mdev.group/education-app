import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { successGetCourse, successGetGlobalCourses, successGetOwnedCourses, successSearchCourse } from '../actions/course';
import { selectGroup, setOwnedGroups } from '../actions/group';
import { successSearchPupil } from '../actions/pupil';
import { ICourse } from '../types/dto/course';

export interface ICourseState {
  searchCourses?: ICourse[];
  ownedCourses?: ICourse[];
  globalCourses?: ICourse[];
  currentCourse?: ICourse;
}

const initialState: ICourseState = {
  searchCourses: undefined,
  ownedCourses: undefined,
  globalCourses: undefined,
  currentCourse: undefined
};

// TODO: remove all any
const reducer = handleActions<ICourseState, any>({
  [successSearchCourse.toString()]: (state, {payload}: Action<ICourse[]>) => ({
    ...state,
    searchCourses: payload
  }),

  [successGetCourse.toString()]: (state, {payload}: Action<ICourse>) => ({
    ...state,
    currentCourse: payload
  }),

  [successGetOwnedCourses.toString()]: (state, {payload}: Action<ICourse[]>) => ({
    ...state,
    ownedCourses: payload
  }),

  [successGetGlobalCourses.toString()]: (state, {payload}: Action<ICourse[]>) => ({
    ...state,
    globalCourses: payload
  }),
}, initialState);

export default reducer;
