import { ReducersMapObject } from 'redux';

import auth, { IAuthState } from './auth';
import course, { ICourseState } from './course';
import group, { IGroupState } from './group';
import pupil, { IPupilState } from './pupil';
import user, { IUsertate } from './user';

export interface IRootReducer{
    auth: IAuthState,
    user: IUsertate,
    group: IGroupState,
    pupil: IPupilState,
    course: ICourseState
}

const reducers: ReducersMapObject<IRootReducer, any> = {
    auth,
    user,
    group,
    pupil,
    course
};

export default reducers;
