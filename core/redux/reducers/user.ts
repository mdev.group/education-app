import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';

import { setUserData, SuccessUpdateNotifs } from '../actions/user';
import { INotification } from '../types/dto/notification';
import { IPupil } from '../types/dto/pupil';
import type { IUserDTO } from '../types/dto/user';

export interface IUsertate {
  userData?: IUserDTO | IPupil;
  notifies?: INotification[];
}

const initialState: IUsertate = {
  userData: undefined,
  notifies: undefined
};

// TODO: remove all any
const reducer = handleActions<IUsertate, any>({
  [setUserData.toString()]: (state, {payload}: Action<IUserDTO>) => ({
    ...state,
    userData: payload
  }),

  [SuccessUpdateNotifs.toString()]: (state, {payload}: Action<INotification[]>) => ({
    ...state,
    notifies: payload
  }),
}, initialState);

export default reducer;
