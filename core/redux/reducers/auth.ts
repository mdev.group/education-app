import { handleActions, Action } from 'redux-actions';

import {
  setRefreshData,
  setRefreshData_payload,
  setRefreshed,
  setToken,
  setToken_payload,
  userAuth,
  userAuthEnd,
  userAuthFail,
  userAuthFail_payload,
  userAuthSuccess, 
  userRegisterFail,
  userRegisterSuccess
} from '../actions/auth';

export interface IRefreshData {
  accessTokenLifeTime: number;
  refreshToken: string;
}

export interface IAuthState {
  token?: string;
  refreshData?: IRefreshData;
  isAuthInProgress: boolean;
  authError?: string;
  registerError?: string;
  refreshed?: boolean;
}

const initialState: IAuthState = {
  token: '',
  isAuthInProgress: false,
  authError: '',
  refreshData: undefined,
  registerError: undefined,
  refreshed: undefined
};

// TODO: remove all any
const reducer = handleActions<IAuthState, any>({
  [setToken.toString()]: (state, {payload}: Action<setToken_payload>) => ({
    ...state,
    token: payload.token
  }),
  [setRefreshData.toString()]: (state, {payload}: Action<setRefreshData_payload>) => ({
    ...state,
    refreshData: payload.refreshData
  }),
  [setRefreshed.toString()]: (state) => ({
    ...state,
    refreshed: true
  }),

  [userAuth.toString()]: (state) => ({
    ...state,
    isAuthInProgress: true
  }),

  [userAuthEnd.toString()]: (state) => ({
    ...state,
    isAuthInProgress: false
  }),

  [userAuthFail.toString()]: (state, {payload}: Action<userAuthFail_payload>) => ({
    ...state,
    authError: payload.error
  }),

  [userAuthSuccess.toString()]: (state) => ({
    ...state,
    authError: undefined
  }),

  [userRegisterFail.toString()]: (state, {payload}: Action<userAuthFail_payload>) => ({
    ...state,
    registerError: payload.error
  }),

  [userRegisterSuccess.toString()]: (state) => ({
    ...state,
    registerError: undefined
  }),
}, initialState);

export default reducer;
