export enum EUserType {
  Teacher,
  Student
}

export interface NotifyItem {
  url?: string;
  text: string;
}

export interface INotification {
  _id: string;
  data: NotifyItem[];
  create_datetime: number;
  userType: EUserType;
  userID: string;
  createdBy: string;
}