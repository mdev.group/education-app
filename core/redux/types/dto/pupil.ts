interface ICourseAssign {
  ID: string;
  taskID: string | null;
}

export interface IPupil {
  _id: string;
  first_name: string;
  last_name: string;
  username: string;
  photo_url?: string;
  courses?: ICourseAssign[]
  birth_date?: number;
  groupID?: string;
}