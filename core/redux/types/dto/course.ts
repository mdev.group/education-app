import { ILevelDTO } from "./level";
import { IUserDTO } from "./user";

export interface ICourse {
  _id: string;
  name: string;
  teacherID: string;
  create_datetime: number;
  levels?: ILevelDTO[];
  minAge: number;
  maxAge: number;
  description: string;
  photo_url: string;
  teacher?: IUserDTO;
  show?: boolean;
}