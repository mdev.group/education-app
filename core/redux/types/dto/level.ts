import { ITaskDTO } from "./task";

export interface ILevelDTO {
  _id: string;
  name: string;
  order: number;
  courseID: string;
  tasks?: ITaskDTO[]
}