enum EFieldItemType {
  clear,
  water
}

interface IPuzzle {
  type: string;
  count: number;
}

interface IFieldItem {
  type: EFieldItemType;
}


export interface ITaskDTO {
  _id: string;
  levelID: string;
  text: string;
  name: string;
  hint: string;
  startPos: string;
  finalPos: string;

  field: IFieldItem[][];
  puzzles: IPuzzle[];
}