import { IPupil } from "./pupil";

export interface IGroupDTO {
  name: string;
  teacherID: string;
  courseID: string;
  _id: string;
  students: IPupil[];
}