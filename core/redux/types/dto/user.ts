export enum EUserRoles {
  Teacher = "Teacher",
  Pupil = "Pupil",
}

export interface IUserDTO {
  first_name: string;
  last_name: string;
  photo_url: string | null;
  tgID: number;
  email: string;
  reg_datetime: number;
  birth_date: number;
  _id: string;
}