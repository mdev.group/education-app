import classNames from "classnames";
import { useForm } from "core/hooks/useForm";
import { beginGetGlobalCourses, beginGetOwnedCourses, beginUpdateCourse } from "core/redux/actions/course";
import { currentUser_selector, globalCourses_selector, isTeacher_selector, ownedCourses_selector } from "core/redux/selectors";
import { ILevelDTO } from "core/redux/types/dto/level";
import Button from "core/uikit/Button";
import CourseCard from "core/uikit/CourseCard/CourseCard";
import Image from "core/uikit/Image";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input/Input";
import ModalContainer from "core/uikit/ModalContainer";
import AppendCourseModal from "core/uikit/page/panel/AppendCourseModal/AppendCourseModal";
import StudentPage from "core/uikit/page/panel/StudentPage";
import ProgressDetails from "core/uikit/page/panel/StudentPage/componetns/ProgressDetails/ProgressDetails";
import TeacherPage from "core/uikit/page/panel/TeacherPage";
import Toggle from "core/uikit/Toggle";
import Typo from "core/uikit/Typo";
import { declOfNum } from "core/utils/devlOfNum";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from "styles/pages/CoursesPage.module.scss";

const Page: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { courseID } = router.query;

  const courses = useSelector(ownedCourses_selector)
  const course = useMemo(() => courses.find((it) => it._id === courseID), [courseID, courses]);
  const currentUser = useSelector(currentUser_selector);

  useEffect(() => {
    if (currentUser) {
      dispatch(beginGetOwnedCourses({
        userType: 'teacher'
      }))

      dispatch(beginGetGlobalCourses())
    }
  }, [dispatch, currentUser]);

  const [levelID, setLevelID] = useState<string | null>(null);

  const handleSave = useCallback((formData) => {
    dispatch(beginUpdateCourse({
      id: course?._id,
      newData: formData
    }))
  }, [course, dispatch])

  const {
    formData,
    reinit,
    onChangeField,
    handleSubmit,
    isChanged
  } = useForm(handleSave, {
    ...course,
  })

  useEffect(() => {
    reinit();
  }, [course])

  const [showAppendModal, setShowModal] = useState(false);

  return (
    <>
      {showAppendModal && (
        <ModalContainer
          onClose={() => setShowModal(false)}
        >
          <AppendCourseModal
            course={course!}
            onClose={() => setShowModal(false)}
          />
        </ModalContainer>
      )}
      {course && formData && (
        <div className={classNames(styles.page, styles['page--row'])}>

          <div className={styles.page__structureColumn}>
            <div className={styles.page__columnTitle}>
              Общая информация
            </div>

            {formData.levels?.map((level: ILevelDTO) => (
              <div
                key={level._id}
                onClick={() => setLevelID(level._id)}
                className={classNames(styles.page__level, {
                  [styles['page__levelTitle--opened']]: levelID == level._id
                })}
              >
                <div className={styles.page__levelTitle}>
                  {level.name}

                  <Image
                    className={styles.page__lavelTitleChevron}
                    src="/images/icons/chevron-bottom.svg"
                    alt=""
                  />
                </div>
                {levelID == level._id && level.tasks?.map((task) => (
                  <div
                    key={task._id}
                    className={styles.page__taskTitle}
                    onClick={() => redirectTo(`/panel/courses/${course._id}/${level._id}/${task._id}`)}
                  >
                    {task.name}
                  </div>
                ))}
              </div>
            ))}

            <Button variant='primary' onClick={() => setShowModal(true)}>
              Дополнить
            </Button>
          </div>

          <div className={styles.page__courseMainColumn}>
            <div className={styles.page__courseMainInfo}>
              <div>
                <Input
                  label="Название курса"
                  value={formData['name']}
                  onChange={onChangeField('name')}
                />
                <div className={styles.page__courseMainInfo__field}>
                  <Typo variant='rubrik'>Возраст</Typo>
                  <div>
                    <span>От</span>
                    <Input
                      value={formData['minAge']}
                      onChange={onChangeField('minAge')}
                    />
                    <span>до</span>
                    <Input
                      value={formData['maxAge']}
                      onChange={onChangeField('maxAge')}
                    />
                    <span>лет</span>
                  </div>
                </div>
                <Input
                  label="Подробное описание"
                  value={formData['description']}
                  onChange={onChangeField('description')}
                  area
                />
              </div>
              <div>
                <ImageUploader
                  className={styles.page__imageUploader}
                  label={(
                    <div className={styles.page__imageUploaderPlaceholder}>
                      {!course.photo_url && <Image src="/images/icons/photo_upload.svg" alt=""/>}
                      <div>
                        <span className={styles['page__text--green']}>Выберите фото</span>
                        <span>{' '}или перетащите в эту область</span>
                      </div>
                    </div>
                  )}
                  onUpload={onChangeField('photo_url')}
                  value={course.photo_url}
                />

                {/* <div className={styles.page__toggleWrapper}>
                  <Typo variant='title-16'>
                    Показывать в списке курсов
                  </Typo>
                  <Toggle
                    value={formData.show}
                    onChange={() => onChangeField('show')(!formData.show as any)}
                  />
                </div> */}
              </div>
            </div>


            <Typo variant="title-20">
              Предпросмотр
            </Typo>

            <div className={styles.page__preview}>
              <CourseCard
                data={formData}
              />
            </div>

            <div className={styles.page__actions}>
              <Button
                variant='primary'
                onClick={handleSubmit}
                disabled={!isChanged}
              >
                Сохранить
              </Button>
              <Button
                variant='secondary-border'
                onClick={() => reinit()}
              >
                Отменить
              </Button>
            </div>
          </div>

        </div>
      )}
    </>
  );
};

export default Page;
