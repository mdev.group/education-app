import { useForm } from "core/hooks/useForm";
import { beginCreateCourse, beginCreateTask, beginGetGlobalCourses, beginGetOwnedCourses } from "core/redux/actions/course";
import { currentUser_selector, globalCourses_selector, isTeacher_selector, ownedCourses_selector } from "core/redux/selectors";
import { ICourse } from "core/redux/types/dto/course";
import Button from "core/uikit/Button";
import CourseCard from "core/uikit/CourseCard/CourseCard";
import Image from "core/uikit/Image";
import Input from "core/uikit/Input/Input";
import ModalContainer from "core/uikit/ModalContainer";
import StudentPage from "core/uikit/page/panel/StudentPage";
import ProgressDetails from "core/uikit/page/panel/StudentPage/componetns/ProgressDetails/ProgressDetails";
import TeacherPage from "core/uikit/page/panel/TeacherPage";
import Typo from "core/uikit/Typo";
import { declOfNum } from "core/utils/devlOfNum";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from "styles/pages/CoursesPage.module.scss";

const Page: NextPage = () => {
  const dispatch = useDispatch();

  const currentUser = useSelector(currentUser_selector);
  const isTeacher = useSelector(isTeacher_selector);

  const globalCourses = useSelector(globalCourses_selector);
  const ownedCourses = useSelector(ownedCourses_selector);

  const [showAll, setShowAll] = useState(true)

  const handleChangeMode = useCallback((value) => () => {
    setShowAll(value)
  }, [])

  useEffect(() => {
    if(currentUser) {
      dispatch(beginGetOwnedCourses({
        userType: isTeacher ? 'teacher' : 'student'
      }))

      dispatch(beginGetGlobalCourses())
    }
  }, [dispatch, isTeacher, currentUser]);

  const [selectedCourse, setSelectedCourse] = useState<string | null>(null);

  const handleSelectCourse = useCallback((course: ICourse| null) => () => {
    if(isTeacher && course?.teacherID == currentUser?._id) {
      redirectTo(`/panel/courses/${course?._id}`);
    } else {
      setSelectedCourse(course?._id || null);
    }
  }, [currentUser, isTeacher])

  const courseData = useMemo(() => selectedCourse && globalCourses.find((item) => item._id === selectedCourse), [globalCourses, selectedCourse])
  const [showСreateModal, setShowCreateModal] = useState(false);

  const handleCreateCourse = useCallback((formData) => {
    dispatch(beginCreateCourse({
      data: formData
    }))

    setShowCreateModal(false);
  }, [dispatch]);

  const {
    formData,
    handleSubmit,
    onChangeField
  } = useForm(handleCreateCourse);

  return (
    <>
      {courseData && (
        <ModalContainer
          onClose={handleSelectCourse(null)}
          className={styles.modal}
        >
          <div className={styles.modal__container}>
            <div className={styles.modal__infoWrapper}>
              <Image
                src={courseData.photo_url}
                alt=""
                className={styles.modal__image}
              />
              <div className={styles.modal__info}>
                <Typo variant='title-16-b'>Курс: {courseData.name}</Typo>
                <Typo variant='title-14'>Рекомендуемый возраст: {courseData.minAge} - {courseData.maxAge} {declOfNum(courseData.maxAge, ['год', 'года', 'лет'])}</Typo>
                <Typo variant='title-14'>Автор: {[courseData.teacher?.first_name, courseData.teacher?.last_name].join(' ')}</Typo>
                <Typo variant='title-14'>Описание: {courseData.description}</Typo>
              </div>
            </div>

            <div>
              <ProgressDetails
                courseData={courseData}
              />
            </div>
          </div>
        </ModalContainer>
      )}
      {showСreateModal && (
        <ModalContainer
          onClose={() => setShowCreateModal(false)}
          className={styles.page__createCourseModal}
        >
          <Typo variant='title-20'>Создание курса</Typo>
          <div className={styles.page__createCourseModalForm}>
            <Input
              label="Название"
              value={formData.name}
              onChange={onChangeField('name')}
            />
          </div>
          <Button variant='primary' onClick={handleSubmit} disabled={!formData.name}>Создать</Button>
        </ModalContainer>
      )}


      <div className={styles.page}>
        <div className={styles.page__container}>
          <div className={styles.page__modeWrapper}>
            <Button onClick={handleChangeMode(true)} variant={showAll ? 'primary' : 'transparent'}>Все курсы</Button>
            <Button onClick={handleChangeMode(false)} variant={!showAll ? 'primary' : 'transparent'}>Мои курсы</Button>
          </div>

          <div className={styles.page__coursesList}>
            {showAll ? (
              globalCourses.filter((course) => course.show).map((course) => (
                <CourseCard
                  key={course._id}
                  data={course}
                  onClick={handleSelectCourse(course)}
                />
              ))
            ) : (
              ownedCourses.map((course) => (
                <CourseCard
                  key={course._id}
                  data={course}
                  onClick={handleSelectCourse(course)}
                />
              ))
            )}
          </div>
          <Button variant='primary' onClick={() => setShowCreateModal(true)}>
            Создать курс
          </Button>
        </div>
      </div>
    </>
  );
};

export default Page;
