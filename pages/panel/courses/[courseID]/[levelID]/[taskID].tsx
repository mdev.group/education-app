import classNames from "classnames";
import { useForm } from "core/hooks/useForm";
import { beginGetGlobalCourses, beginGetOwnedCourses, beginUpdateCourse, beginUpdateTask } from "core/redux/actions/course";
import { currentUser_selector, globalCourses_selector, isTeacher_selector, ownedCourses_selector } from "core/redux/selectors";
import { toolbox } from "core/uikit/Blockly/constants";
import Button from "core/uikit/Button";
import CourseCard from "core/uikit/CourseCard/CourseCard";
import Image from "core/uikit/Image";
import Input from "core/uikit/Input/Input";
import ModalContainer from "core/uikit/ModalContainer";
import { mapCeilTypeToClassName } from "core/uikit/page/constants";
import AppendCourseModal from "core/uikit/page/panel/AppendCourseModal/AppendCourseModal";
import StudentPage from "core/uikit/page/panel/StudentPage";
import ProgressDetails from "core/uikit/page/panel/StudentPage/componetns/ProgressDetails/ProgressDetails";
import TeacherPage from "core/uikit/page/panel/TeacherPage";
import Select from "core/uikit/Select";
import Toggle from "core/uikit/Toggle";
import Typo from "core/uikit/Typo";
import { declOfNum } from "core/utils/devlOfNum";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from "styles/pages/CoursesPage.module.scss";
import taskstyles from "styles/pages/TaskPage.module.scss";

const Page: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { courseID, levelID: QueryLevelID, taskID: QueryTaskID } = router.query;

  const courses = useSelector(ownedCourses_selector)
  const course = useMemo(() => courses.find((it) => it._id === courseID), [courseID, courses]);
  const currentUser = useSelector(currentUser_selector);

  const level = useMemo(() => course?.levels?.find((it) => it._id === QueryLevelID), [QueryLevelID, course])
  const task = useMemo(() => level?.tasks?.find((it) => it._id === QueryTaskID), [QueryTaskID, level]);

  useEffect(() => {
    if (currentUser) {
      dispatch(beginGetOwnedCourses({
        userType: 'teacher'
      }))

      dispatch(beginGetGlobalCourses())
    }
  }, [dispatch, currentUser]);

  const [levelID, setLevelID] = useState<string | null>(QueryLevelID as string);

  const handleSave = useCallback((formData) => {
    dispatch(beginUpdateTask({
      id: task?._id,
      newData: formData
    }))
  }, [dispatch, task])

  const {
    formData,
    reinit,
    onChangeField,
    handleSubmit,
    isChanged
  } = useForm(handleSave, {
    ...task,
  })

  useEffect(() => {
    reinit()
  }, [task])

  const [selectedField, setSelectedField] = useState<string | null>(null)

  const updateField = useCallback((pos, type) => () => {
    const [x, y] = pos.split(';');
    if(type == 'start') {
      onChangeField(`startPos`)(pos);
      setSelectedField(null);
      return ;
    }

    if(type == 'end') {
      onChangeField(`finalPos`)(pos);
      setSelectedField(null);
      return ;
    }

    onChangeField(`field.${x}.${y}.type`)(type);
    setSelectedField(null);
  }, [onChangeField])

  const [showAppendModal, setShowModal] = useState(false);


  return (
    <>
      {showAppendModal && (
        <ModalContainer
          onClose={() => setShowModal(false)}
        >
          <AppendCourseModal
            course={course!}
            onClose={() => setShowModal(false)}
          />
        </ModalContainer>
      )}
      {course && (
        <div className={classNames(styles.page, styles['page--row'])}>

          <div className={styles.page__structureColumn}>
            <div className={classNames(styles.page__columnTitle, styles['page__columnTitle--white'])} onClick={() => redirectTo(`/panel/courses/${course._id}`)}>
              Общая информация
            </div>

            {course.levels?.map((level) => (
              <div
                key={level._id}
                className={classNames(styles.page__level, {
                  [styles['page__levelTitle--active']]: QueryLevelID == level._id,
                  [styles['page__levelTitle--opened']]: QueryLevelID == level._id || levelID == level._id
                })}
                onClick={() => setLevelID(level._id)}
              >
                <div className={styles.page__levelTitle}>
                  {level.name}

                  <Image
                    className={styles.page__lavelTitleChevron}
                    src="/images/icons/chevron-bottom.svg"
                    alt=""
                  />
                </div>
                {(levelID == level._id || QueryLevelID == level._id) && level.tasks?.map((task) => (
                  <div
                    key={task._id}
                    className={classNames(styles.page__taskTitle, {
                      [styles['page__taskTitle--active']]: QueryTaskID === task._id
                    })}
                    onClick={() => redirectTo(`/panel/courses/${course._id}/${level._id}/${task._id}`)}
                  >
                    {task.name}
                  </div>
                ))}
              </div>
            ))}

            <Button variant='primary' onClick={() => setShowModal(true)}>
              Дополнить
            </Button>
          </div>

          <div className={styles.page__courseMainColumn}>
            {task && formData && (
              <>
                  <div className={styles.page__courseMainInfo}>
                    <div className={styles.page__taskColumn}>
                      <Input
                        label="Название задания"
                        value={formData['name']}
                        onChange={onChangeField('name')}
                      />
                      <Input
                        label="Формулировка задания"
                        value={formData['text']}
                        onChange={onChangeField('text')}
                        area
                      />
                      <Input
                        label="Подсказка"
                        value={formData['hint']}
                        onChange={onChangeField('hint')}
                      />
                      <Select
                        className={styles.page__selector}
                        label="Размер игрового поля"
                        value={(formData.field?.length && formData.field[0]?.length) ? `${formData.field?.length} X ${formData.field[0]?.length}` : 'Выберите...'}
                        items={[
                          {
                            label: "5 X 5",
                            onClick: () => {
                              onChangeField('field')(new Array(5).fill(new Array(5).fill({type: 2})) as any)
                            }
                          },
                          {
                            label: "6 X 6",
                            onClick: () => {
                              onChangeField('field')(new Array(6).fill(new Array(6).fill({type: 2})) as any)
                            }
                          },
                          {
                            label: "7 X 7",
                            onClick: () => {
                              onChangeField('field')(new Array(7).fill(new Array(7).fill({type: 2})) as any)
                            }
                          }
                        ]}
                      />

                      {selectedField && (
                        <div className={styles.page__chooseField}>
                          <Typo variant='title-16'>
                            Выберите тип клетки
                          </Typo>
                          <div>
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: selectedField == formData.startPos
                              })}
                              src="/images/gameChar.png"
                              alt=""
                              onClick={updateField(selectedField, 'start')}
                            />
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: selectedField == formData.finalPos
                              })}
                              src="/images/blocks/bone.png"
                              alt=""
                              onClick={updateField(selectedField, 'end')}
                            />
                          </div>

                          <div>
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: formData.field[selectedField.split(';')[0]][selectedField.split(';')[1]].type == '1'
                              })}
                              src="/images/blocks/water.svg"
                              alt=""
                              onClick={updateField(selectedField, 1)}
                            />
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: formData.field[selectedField.split(';')[0]][selectedField.split(';')[1]].type == '2'
                              })}
                              src="/images/blocks/ground.svg"
                              alt=""
                              onClick={updateField(selectedField, 2)}
                            />
                          </div>

                          <div>
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: formData.field[selectedField.split(';')[0]][selectedField.split(';')[1]].type == '4'
                              })}
                              src="/images/blocks/cat.png"
                              alt=""
                              onClick={updateField(selectedField, 4)}
                            />
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: formData.field[selectedField.split(';')[0]][selectedField.split(';')[1]].type == '5'
                              })}
                              src="/images/blocks/cactus.png"
                              alt=""
                              onClick={updateField(selectedField, 5)}
                            />
                            <Image
                              className={classNames(styles.page__chooseFieldItem, {
                                [styles['page__chooseFieldItem--active']]: formData.field[selectedField.split(';')[0]][selectedField.split(';')[1]].type == '3'
                              })}
                              src="/images/blocks/bridge.svg"
                              alt=""
                              onClick={updateField(selectedField, 3)}
                            />
                          </div>

                          <Typo variant='title-14'>*через кота можно пройти, надев на собаку маску;<br/>через кактус и воду пройти нельзя никогда</Typo>
                        </div>
                      )}
                    </div>
                    <div>
                      <div className={classNames(taskstyles.page__column)}>
                        <div className={taskstyles.task}>
                          {formData.field?.map((item: any, xindex: number) => (
                            <div key={xindex} className={taskstyles.task__row}>
                              {item.map((item: any, yindex: number) => {
                                const [xstart, ystart] = formData.startPos.split(';');
                                const [xend, yend] = formData.finalPos.split(';');

                                let isStart = Number(xstart) == xindex && Number(ystart) == yindex;
                                let isEnd = Number(xend) == xindex && Number(yend) == yindex;

                                return (
                                  <div key={yindex} className={
                                    classNames(
                                      'gameCell',
                                      taskstyles.task__ceil,
                                      taskstyles.field,
                                      styles.page__field,
                                      {
                                        [styles['page__field--active']]: selectedField == `${xindex};${yindex}`
                                      }
                                    )}
                                      onClick={() => {
                                        setSelectedField(`${xindex};${yindex}`)
                                      }}
                                    >
                                    <div
                                      className={classNames(
                                        taskstyles.field__item,
                                        mapCeilTypeToClassName(item.type)
                                      )}
                                    />
                                    {isStart && <div id='gameChar'></div>}
                                    {isEnd && <div id='gameEnd'></div>}
                                  </div>
                                )
                              })}
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>



                    <div className={styles.fill}>
                      <div className={styles.page__taskPuzzlesCard}>
                        <Typo variant="title-16-b">Пазлы для прохождения задания</Typo>
                        <div className={styles.page__taskPuzzlesList}>
                          {toolbox.contents.map((item) => (
                            <div key={item.type}
                              className={classNames(styles.page__taskPuzzlesListItem, {
                                [styles['page__taskPuzzlesListItem--disabled']]: !formData.puzzles?.some((puz: any) => item.type === puz.type)
                              })}
                            >
                              <Input
                                type="checkbox"
                                value={formData.puzzles?.some((puz: any) => item.type === puz.type)}
                                onChange={(oldValue) => {
                                  if(oldValue == 'false') {
                                    const puzzles = [...formData.puzzles || []];
                                    puzzles.push({
                                      type: item.type,
                                      count: 1
                                    })
                                    onChangeField('puzzles')(puzzles as any)
                                  } else {
                                    let puzzles = [...formData.puzzles || []];
                                    puzzles = puzzles.filter((puz) => puz.type !== item.type)
                                    onChangeField('puzzles')(puzzles as any)
                                  }
                                }}
                              />
                              <Image
                                src={`/images/puzzles/${item.type}.png`}
                                alt=""
                                className={styles.page__taskPuzzlesListItemIcon}
                              />
                              <div className={styles.page__taskPuzzlesListItemName}>
                                <Typo variant="title-14">{item.name}</Typo>
                              </div>
                              <div className={styles.page__taskPuzzlesListItemCountWrapper}>
                                <Typo variant="title-14">Количество пазлов</Typo>
                                <Input
                                  className={styles.page__taskPuzzlesListItemCount}
                                  placeholder="0"
                                  onChange={(newValue) => {
                                    const index = formData.puzzles?.findIndex((puz: any) => puz.type === item.type);
                                    if(index !== -1) {
                                      onChangeField(`puzzles.${index}.count`)(newValue as any)
                                    }
                                  }}
                                  value={formData.puzzles?.find((puz: any) => puz.type === item.type)?.count || '0'}
                                />
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className={styles.page__actions}>
                    <Button
                      variant='primary'
                      onClick={handleSubmit}
                      disabled={!isChanged}
                    >
                      Сохранить
                    </Button>
                    <Button
                      variant='secondary-border'
                      onClick={() => reinit()}
                    >
                      Отменить
                    </Button>
                  </div>
              </>
            )}
          </div>

        </div>
      )}
    </>
  );
};

export default Page;
