
import classNames from "classnames";
import { beginGetGlobalCourses, beginSolveTask } from "core/redux/actions/course";
import { currentUser_selector, globalCourses_selector } from "core/redux/selectors";
import { IPupil } from "core/redux/types/dto/pupil";
import BlocklyPanel from "core/uikit/Blockly/Blockly";
import Button from "core/uikit/Button";
import Image from "core/uikit/Image";
import ProgressDetails from "core/uikit/page/panel/StudentPage/componetns/ProgressDetails/ProgressDetails";
import Typo from "core/uikit/Typo";
import { getActiveTask } from "core/utils/getScoresByTaskID";
import redirectTo from "core/utils/redirectTo";

import type { NextPage } from "next";
import { useRouter } from "next/router";
import { CSSProperties, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from "styles/pages/TaskPage.module.scss";
import { mapCeilTypeToClassName } from "core/uikit/page/constants";


const Page: NextPage = () => {
  const {query: {id}} = useRouter();
  const dispatch = useDispatch();

  const currentUser = useSelector(currentUser_selector) as IPupil;
  const globalCourses = useSelector(globalCourses_selector);

  useEffect(() => {
    dispatch(beginGetGlobalCourses())
  }, [dispatch]);

  const currentTask = useMemo(() => {
    return globalCourses.map((item) => (item.levels)).flat(2).map((item) => item?.tasks).flat(2).find((task) => task?._id === id)
  }, [globalCourses, id])

  
  const currentCourse = useMemo(() => {
    const currentLevel = globalCourses.map((item) => (item.levels)).flat(2).find((item) => item?._id === currentTask?.levelID)
    return globalCourses.find((item) => item._id == currentLevel?.courseID)
  }, [globalCourses, currentTask])

  const [showHint, setShowHint] = useState(false);

  const handleUpdateShowHint = useCallback((value) => () => {
    setShowHint(value);
  }, [])

  useEffect(() => {
    if(currentTask){
      const [y, x] = currentTask.finalPos.split(';');
      const [ystart, xstart] = currentTask.startPos.split(';');
      window.gameWin = {
        startX: Number(xstart),
        startY: Number(ystart),
        posX: Number(x),
        posY: Number(y),
        fields: currentTask.field
      } 
    }
  })

  const {
    level,
    task,
    levelProgress
  } = useMemo(() => {
    if(currentUser && currentCourse) { return getActiveTask(currentUser, currentCourse) }

    return {
      level: null,
      task: null,
      levelProgress: 0
    };
  }, [currentCourse, currentUser])

  const handleNextLvl = useCallback(() => {
    redirectTo(`/panel/task/${task?._id}`)
  }, [task])

  
  const onSuccess = useCallback(() => {
    dispatch(beginSolveTask({
      id: currentTask?._id
    }))
  }, [dispatch, currentTask])

  const [toolboxWidth, setToolboxWidth] = useState(50);

  return (
    <div className={styles.page}>
      {currentTask && (
        <>
          <div className={styles.page__blockly} style={{'--toolbox': `${toolboxWidth}px`} as CSSProperties}>
            <div className={styles.page__blockyTitles}>
              <div className={styles.page__columnTitle} style={{width: toolboxWidth}}>
                <Typo variant='title-24'>Пазлы</Typo>
              </div>
              <div className={styles.page__columnTitle}>
                <Typo variant='title-24'>Конструктор</Typo>
              </div>
            </div>
            {handleNextLvl && onSuccess && (
              <BlocklyPanel
                handleNextLvl={handleNextLvl}
                onSuccess={onSuccess}
                puzzles={currentTask?.puzzles}
                updateToolboxWidth={setToolboxWidth}
              />
            )}
          </div>

          <div className={classNames(styles.page__column, styles.page__columnTask)}>
            <div className={styles.task}>
              {currentTask.field.map((item, xindex) => (
                <div key={xindex} className={styles.task__row}>
                  {item.map((item, yindex) => {
                    const [xstart, ystart] = currentTask.startPos.split(';');
                    const [xend, yend] = currentTask.finalPos.split(';');
                    
                    let isStart = Number(xstart) == xindex && Number(ystart) == yindex;
                    let isEnd = Number(xend) == xindex && Number(yend) == yindex;
                    
                    return (
                      <div key={yindex} className={
                        classNames(
                          'gameCell',
                          styles.task__ceil,
                          styles.field,
                        )}>
                        <div
                          className={classNames(
                            styles.field__item,
                            mapCeilTypeToClassName(item.type)
                          )}
                        />
                        {isStart && <div id='gameChar'></div>}
                        {isEnd && <div id='gameEnd'></div>}
                      </div>
                    )
                  })}
                </div>
              ))}

              {showHint && <div className={styles.task__hint}>
                <Typo variant='title-14'>{currentTask.hint}</Typo>
              </div>}
            </div>

            <div className={styles.task__infoWrapper}>
              <Button variant='transparent' className={styles.task__hintBtn} onClick={handleUpdateShowHint(!showHint)}>
                <Image
                  src="/images/icons/hint.svg"
                  alt=""
                />
              </Button>

              <Typo variant='title-24'>Привет, {currentUser?.first_name}!</Typo>
              <div className={styles.task__info}>
                <div className={styles.task__infoDectription}>
                  <Typo variant='title-16'>{currentTask.name}</Typo>
                  <Typo variant='title-14'>{currentTask.text}</Typo>
                </div>
                <ProgressDetails
                  courseData={currentCourse}
                  classes={{
                    main: styles.task__progress,
                    list: styles.task__progressList
                  }}
                  showAction={false}
                  showTitle={false}
                  showNames={false}
                />
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Page;
