import { beginGetOwnedCourses } from "core/redux/actions/course";
import { getOwnedGroups } from "core/redux/actions/group";
import { isTeacher_selector } from "core/redux/selectors";
import StudentPage from "core/uikit/page/panel/StudentPage";
import TeacherPage from "core/uikit/page/panel/TeacherPage";
import type { NextPage } from "next";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const Page: NextPage = () => {
  const dispatch = useDispatch();

  const isTeacher = useSelector(isTeacher_selector);
  
  return (
    <div>
      {isTeacher ? (
        <TeacherPage/>
      ) : (
        <StudentPage/>
      )}
    </div>
  );
};

export default Page;
