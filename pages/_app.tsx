/* eslint-disable @next/next/no-sync-scripts */
/* eslint-disable react/jsx-no-comment-textnodes */
import "../styles/globals.scss";
import type { AppProps } from "next/app";
import Head from "next/head";
import { initStore, getStore } from "../core/redux/index";
import { Provider } from "react-redux";
import { useEffect, useRef, useState } from "react";
import authTokenService from "core/services/authTokenService";
import { TStore } from "core/redux/storeDataProvider";
// import AppBar from "core/uikit/AppBar";
import { useRouter } from "next/router";
import Appbar from "core/uikit/Appbar/Appbar";

import 'core/uikit/Blockly/Blockly.scss';

function MyApp({ Component, pageProps }: AppProps) {
  const [store, setStore] = useState<TStore>();

  useEffect(() => {
    initStore();
    const store = getStore();
    authTokenService.init(store.dispatch);
    setStore(store);
  }, [])

  const router = useRouter();

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover"
        />
        <link rel="manifest" href="manifest.json"/>
        <script type="module" src="/pwabuilder-sw-register.js"/>
        <title>ZeroCode.Puzzle</title>
      </Head>
      {store && (
        <Provider store={store}>
          {router.asPath.indexOf('/auth') === -1 && <Appbar />}
          <Component {...pageProps} />
        </Provider>
      )}
    </>
  );
}

export default MyApp;
