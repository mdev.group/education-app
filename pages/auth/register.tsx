import Image from "core/uikit/Image";
import RegisterPanel from "core/uikit/page/auth/RegisterPanel";
import type { NextPage } from "next";

import styles from 'styles/pages/AuthPage.module.scss';

const Page: NextPage = () => {
  return (
    <main className={styles.main}>
      <RegisterPanel/>
      <Image className={styles.main__cat} src="/images/pages/auth/cat.png" alt="" />
    </main>
  );
};

export default Page;
