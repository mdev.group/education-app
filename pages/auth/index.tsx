import authTokenService from "core/services/authTokenService";
import Image from "core/uikit/Image";
import AuthPanel from "core/uikit/page/auth/AuthPanel";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect } from "react";

import styles from 'styles/pages/AuthPage.module.scss';

const Page: NextPage = () => {
  return (
    <>
      <main className={styles.main}>
        <AuthPanel/>
        <Image className={styles.main__cat} src="/images/pages/auth/cat.png" alt="" />
      </main>
    </>
  );
};

export default Page;
